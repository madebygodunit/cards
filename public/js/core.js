// HENIKA -> [ vv ninja ] web.r1

import * as THREE from './three.module.min.js';
import { SUBTRACTION, INTERSECTION, ADDITION, DIFFERENCE, Brush, Evaluator } from './index.module.js';
import * as BufferGeometryUtils from './BufferGeometryUtils.js';
import { FontLoader } from './FontLoader.js';
import { SVGLoader } from './SVGLoader.js';
import { TrailRenderer } from './TrailRenderer.js';
const mainRenderer = new THREE.WebGLRenderer({ antialias: true, powerPreference: "high-performance" });
mainRenderer.outputColorSpace = THREE.SRGBColorSpace;
mainRenderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(mainRenderer.domElement);
const mainScene = new THREE.Scene();
mainScene.background = new THREE.Color(0x2DBE64);


const ambientLight = new THREE.AmbientLight(0xFFFFFF, 0.6);
const directionalLight = new THREE.DirectionalLight(0xFFFFFF, 0.8);
directionalLight.position.set(-1, 1, 1.5);
const topContainer = new THREE.Object3D();
const bottomContainer = new THREE.Object3D();
mainScene.add(ambientLight, directionalLight, topContainer, bottomContainer);
const mainCamera = new THREE.OrthographicCamera(document.body.clientWidth / -2, window.innerWidth / 2, document.body.clientHeight / 2, document.body.clientHeight / -2, 0.01, 40000);
mainCamera.position.z = 10;
const graphics = {};
let oldWindow;
onWindowResize();
function onWindowResize() {
	if (document.body.clientHeight / document.body.clientWidth < 16 / 9) {
	  mainScene.scale.set(document.body.clientHeight / 760, document.body.clientHeight / 760, document.body.clientHeight / 760);
	} else {
		mainScene.scale.set(document.body.clientWidth / 360, document.body.clientWidth / 360, document.body.clientWidth / 360);
	}
	topContainer.position.y = document.body.clientHeight / 2 / mainScene.scale.x;
  bottomContainer.position.y = -document.body.clientHeight / 2 / mainScene.scale.x;
	mainRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
	mainCamera.left = -document.body.clientWidth / 2;
	mainCamera.right = document.body.clientWidth / 2;
	mainCamera.top = document.body.clientHeight / 2;
	mainCamera.bottom = -document.body.clientHeight / 2;
	mainCamera.updateProjectionMatrix();
	oldWindow = document.body.clientWidth / document.body.clientHeight;
}
let preloadingCount = 3;
let EuclidRegular;
new FontLoader().load('./fonts/EuclidRegular.json', function(font) {
	EuclidRegular = font;
	preloadingCount == 0 ? goLoadingScreen() : preloadingCount--;
});
const svgPath = [];
for (let i = 0; i < 3; i++) {
  loadSVG(`./svg/svg${i}.svg`).then(data => {
	  svgPath[i] = data.paths;
  	preloadingCount == 0 ? goLoadingScreen() : preloadingCount--;
  });
}
function goLoadingScreen() {
	graphics.logo = new THREE.Object3D();
	graphics.logo.color = new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 1 });
	for (let i = 0; i < svgPath[0].length; i++) {
		const shapes = svgPath[0][i].toShapes(true);
		for (let j = 0; j < shapes.length; j++) {
			const mesh = new THREE.Mesh(new THREE.ShapeGeometry(shapes[j], 24), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
			graphics.logo.add(mesh);
		}
	}
	graphics.loadingBar = new THREE.Object3D();
	graphics.loadingBar.back = new THREE.Mesh(new THREE.PlaneGeometry(), new THREE.MeshBasicMaterial({ color: 0xD7144B }));
  graphics.loadingBar.back.position.x = -120;
  graphics.loadingBar.line = [];
  graphics.loadingBar.lineBack = [];
  let shape = new THREE.Shape();
  shape.absarc(7, 13.25, 3, 0, Math.PI * 0.5);
  shape.absarc(-7, 13.25, 3, Math.PI * 0.5, Math.PI);
  shape.absarc(-7, -13.25, 3, Math.PI, Math.PI * 1.5);
  shape.absarc(7, -13.25, 3, Math.PI * 1.5, Math.PI * 2);
  for (let i = 0; i < 10; i++) {
    graphics.loadingBar.line[i] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
    graphics.loadingBar.line[i].position.set(-108 + 24 * i, 0, 0.2);
    gsap.to(graphics.loadingBar.line[i].material, { duration: 0.35, opacity: 0.2, ease: "power1.inOut", delay: 0.05 * i });
    graphics.loadingBar.add(graphics.loadingBar.line[i]);
  }
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-7, -13.25, 3, Math.PI, Math.PI * 1.5);
  shape.absarc(7, -13.25, 3, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(7, -10.25, 3, Math.PI * 2, Math.PI * 1.5, true);
  shape.absarc(-7, -10.25, 3, Math.PI * 1.5, Math.PI, true);
  for (let i = 0; i < 10; i++) {
  	graphics.loadingBar.lineBack[i] = new THREE.Mesh(new THREE.ShapeGeometry(shape, 6), new THREE.MeshBasicMaterial({ color: 0xBDBDBD }));
  	graphics.loadingBar.lineBack[i].position.set(-108 + 24 * i, 0, 0.1);
    graphics.loadingBar.lineBack[i].visible = false;
  	graphics.loadingBar.add(graphics.loadingBar.lineBack[i]);
  }
	const loadigBackTimer = { step: 0 };
  gsap.to(loadigBackTimer, { duration: 0.7, step: 240, ease: "power2.out", onUpdate() {
  	graphics.loadingBar.back.geometry.dispose();
  	const shape = new THREE.Shape();
  	shape.absarc(0, 17.5, 9, Math.PI * 0.5, Math.PI);
  	shape.absarc(0, -17.5, 9, Math.PI, Math.PI * 1.5);
  	shape.absarc(loadigBackTimer.step, -17.5, 9, Math.PI * 1.5, Math.PI * 2);
    shape.absarc(loadigBackTimer.step, 17.5, 9, Math.PI * 2, Math.PI * 2.5);
    graphics.loadingBar.back.geometry = new THREE.ShapeGeometry(shape, 8);
  } });
  graphics.loadingBar.countText = new THREE.Mesh(new THREE.ShapeGeometry(EuclidRegular.generateShapes(`0%`, 12), 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
  graphics.loadingBar.countText.geometry.center();
  graphics.loadingBar.countText.position.y = 53;
  graphics.loadingBar.add(graphics.loadingBar.back, graphics.loadingBar.countText);
  graphics.loadingBar.position.set(0, -45, -5);
	graphics.logo.scale.set(1, -1, 1);
  graphics.logo.position.set(-90, -document.body.clientHeight / 2 / mainScene.scale.x + 80, -5);
  gsap.to(graphics.logo.children[4].material, { duration: 0.35, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[1].material, { duration: 0.4, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[3].material, { duration: 0.45, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[5].material, { duration: 0.5, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[0].material, { duration: 0.55, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[6].material, { duration: 0.6, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[2].material, { duration: 0.65, opacity: 1, ease: "power1.inOut" });
  gsap.to(graphics.logo.children[7].material, { duration: 0.7, opacity: 1, ease: "power1.inOut" });
  gsap.from(graphics.logo.children[4].position, { duration: 0.35, y: 20, ease: "back.out" });
  gsap.from(graphics.logo.children[1].position, { duration: 0.4, y: 20, ease: "back.out" });
  gsap.from(graphics.logo.children[3].position, { duration: 0.45, y: 20, ease: "back.out" });
  gsap.from(graphics.logo.children[5].position, { duration: 0.5, y: 20, ease: "back.out", onComplete() {
  	gsap.to(graphics.loadingBar.countText.material, { duration: 0.2, opacity: 1, ease: "power1.inOut" });
  } });
  gsap.from(graphics.logo.children[0].position, { duration: 0.55, y: 20, ease: "back.out" });
  gsap.from(graphics.logo.children[6].position, { duration: 0.6, y: 20, ease: "back.out" });
  gsap.from(graphics.logo.children[2].position, { duration: 0.65, y: 20, ease: "back.out" });
  gsap.from(graphics.logo.children[7].position, { duration: 0.7, y: 20, ease: "back.out", onComplete() {
    goLoad();
  } });
  graphics.backApple = [];
  graphics.backApple[0] = new THREE.Mesh(new THREE.ShapeGeometry(svgPath[1][0].toShapes(true), 24), new THREE.MeshBasicMaterial({ color: 0x4EC87D }));
  graphics.backApple[0].geometry.translate(-50, -50, 0);
  graphics.backApple[0].rotation.z = -2.7;
  graphics.backApple[0].scale.set(1.6, -1.6, 1);
  graphics.backApple[0].position.set(-73, 242, -20);
  graphics.backApple[1] = new THREE.Mesh(new THREE.ShapeGeometry(svgPath[2][0].toShapes(true), 24), new THREE.MeshBasicMaterial({ color: 0x4EC87D }));
  graphics.backApple[1].geometry.scale(0.3, 0.3, 1);
  graphics.backApple[1].geometry.translate(-70, -70, 0);
  graphics.backApple[1].position.set(167, 213, -20);
  graphics.backApple[1].rotation.z = 2.9;
  graphics.backApple[1].scale.set(1.3, -1.3, 1);
  graphics.backApple[2] = graphics.backApple[0].clone();
  graphics.backApple[2].position.set(130, -219, -20);
  graphics.backApple[2].rotation.z = -1.5;
  graphics.backApple[2].scale.set(0.7, 0.7, 1);
  graphics.backApple[3] = graphics.backApple[1].clone();
  graphics.backApple[3].position.set(-30, -310, -20);
  graphics.backApple[3].rotation.z = 2.1;
  graphics.backApple[3].scale.set(1.6, 1.6, 1);
  graphics.backApple[4] = graphics.backApple[0].clone();
  graphics.backApple[4].position.set(-220, -180, -20);
  graphics.backApple[4].rotation.z = 0;
  graphics.backApple[4].scale.set(1.6, 1.6, 1);
  graphics.backApple[5] = graphics.backApple[1].clone();
  graphics.backApple[5].position.set(-210, 380, -20);
  graphics.backApple[5].rotation.z = 2;
  graphics.backApple[5].scale.set(2, 2, 1);
  for (let i = 0; i < 6; i++) {
    gsap.from(graphics.backApple[i].scale, { duration: 0.3 + Math.random() * 0.4, x: 0, y: 0, ease: "back.out" });
    gsap.from(graphics.backApple[i].position, { duration: 0.3 + Math.random() * 0.4, x: 0, y: 0, ease: "back.out" });
  }
  topContainer.add(graphics.logo);
	mainScene.add(graphics.loadingBar, graphics.backApple[5], graphics.backApple[4], graphics.backApple[0], graphics.backApple[1], graphics.backApple[2], graphics.backApple[3]);
}




function loadSVG(url) { return new Promise(resolve => { new SVGLoader().load(url, resolve) }) }
function clearTempGeometries() { while (tempGeometry.length > 0) { tempGeometry[tempGeometry.length - 1].dispose(); tempGeometry.pop(); } }
function bendGeometry(geometry, axis, angle) { let theta = 0; if (angle !== 0) { const v = geometry.attributes.position.array; for (let i = 0; i < v.length; i += 3) { let x = v[i]; let y = v[i + 1]; let z = v[i + 2]; switch (axis) { case "x": theta = z * angle; break; case "y": theta = x * angle; break; default: theta = x * angle; break; } let sinTheta = Math.sin(theta); let cosTheta = Math.cos(theta); switch (axis) { case "x": v[i] = x; v[i + 1] = (y - 1.0 / angle) * cosTheta + 1.0 / angle; v[i + 2] = -(y - 1.0 / angle) * sinTheta; break; case "y": v[i] = -(z - 1.0 / angle) * sinTheta; v[i + 1] = y; v[i + 2] = (z - 1.0 / angle) * cosTheta + 1.0 / angle; break; default: v[i] = -(y - 1.0 / angle) * sinTheta; v[i + 1] = (y - 1.0 / angle) * cosTheta + 1.0 / angle; v[i + 2] = z; break; } } geometry.attributes.position.needsUpdate = true; } }




const tex = [];
let loadingCount = 34;
let Villula;
let EuclidBold;
function goLoad() {
	for (let i = 0; i < 30; i++) {
		loadPIC(`textures/tex${i}.png`).then(texture => {
			tex[i] = texture;
			tex[i].colorSpace = THREE.SRGBColorSpace;
      checkLoading();
		});
	}
	new FontLoader().load('./fonts/Villula.json', function(font) {
		Villula = font;
		checkLoading();
	});
	new FontLoader().load('./fonts/EuclidBold.json', function(font) {
		EuclidBold = font;
		checkLoading();
	});
	for (let i = 3; i < 4; i++) {
		loadSVG(`./svg/svg${i}.svg`).then(data => {
			svgPath[i] = data.paths;
			checkLoading();
		});
	}
}

function checkLoading() {
	
	loadingCount--;
//	game.loadingCountText.geometry.dispose();
//	game.loadingCountText.geometry = new THREE.ShapeGeometry(GolosRegular.generateShapes(`${Math.round(100 / loadingFull * (loadingFull - loadingCount))}%`, 10), 4);
//	game.loadingCountText.geometry.center();

  
	if (loadingCount == 1) {
		createGraphics();
	} else if (loadingCount == 0) {
  	hideLoadingScreen();
  }
}


function loadPIC(url) { return new Promise(resolve => { new THREE.TextureLoader().load(url, resolve) }) }


const format = (mainRenderer.capabilities.isWebGL2) ? THREE.RedFormat : THREE.LuminanceFormat;
const colors = new Uint8Array(2);
for (let c = 0; c <= colors.length; c++) {
	colors[c] = (c / colors.length) * 256;
}
const gradient = new THREE.DataTexture(colors, colors.length, 1, format);
gradient.needsUpdate = true;
const tempGeometry = [];
let graphicsReady = false;
const headGeometry = [new THREE.Vector3(-8, -16, 0), new THREE.Vector3(0, 0, 0), new THREE.Vector3(8, -16, 0)];
const starShape = new THREE.Shape();
starShape.moveTo(-1, 0);
starShape.lineTo(1, 0);
const starGeometry = new THREE.ShapeGeometry(starShape, 1);
const trailTargetMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff });
const trailTarget = new THREE.Mesh(starGeometry, trailTargetMaterial);
trailTarget.position.set(0, 0, 0);
trailTarget.scale.multiplyScalar(0.5);
trailTarget.matrixAutoUpdate = false;
mainScene.add(trailTarget);
trailTarget.oldPos;
const trail = new TrailRenderer(mainScene, false);
trail.setAdvanceFrequency(400);
const trailMaterial = TrailRenderer.createBaseMaterial();
trailMaterial.uniforms.headColor.value.set(1, 1, 1, 0.5);
trailMaterial.uniforms.tailColor.value.set(1, 1, 1, 0);
const transparentMaterial = new THREE.MeshBasicMaterial({ transparent: true, opacity: 0.0000001 })
let onCut = false;
let onFly = false;
let onTrail = false;
let onGame = false;
let onTutorial = 0;
const text = [
	[`НареЖь`, `и СъЕшЬ`],
	[`Есть натуральное — сложная задача!`,
	 `Консерванты и усилители вкуса только`,
	 `и ждут, чтобы попасть в тарелку`,
	 `Не дай им пройти: собери обед без`,
	 `вредных добавок`],
	[`Разрезай летящие продукты`,
	 `в большую тарелку`],
	[`Следи, чтобы туда не попали`,
	 `усилители вкуса и консерванты`]
]
const darkRedColor = new THREE.Color(0xA51D43);
const redShadowColor = new THREE.Color(0x7D1633);
function createGraphics() {
	graphics.title = new THREE.Object3D();
	graphics.title.line_1 = new THREE.Object3D();
	graphics.title.line_1.symbol = [];
	for (let j = text[0][0].length - 1; j >= 0; j--) {
		const lineLength = new THREE.ShapeGeometry(Villula.generateShapes(text[0][0].slice(0, j + 1), 32.2), 1);
		lineLength.computeBoundingBox();
		graphics.title.line_1.symbol[j] = new THREE.Mesh(new THREE.ShapeGeometry(Villula.generateShapes(text[0][0].slice(j, j + 1), 32.2), 8), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
		graphics.title.line_1.symbol[j].geometry.computeBoundingBox();
		graphics.title.line_1.symbol[j].geometry.translate(lineLength.boundingBox.max.x - graphics.title.line_1.symbol[j].geometry.boundingBox.max.x, 0, 0);
    graphics.title.line_1.add(graphics.title.line_1.symbol[j]);
	}
	graphics.title.line_1.position.x = -98.5;
	graphics.title.line_2 = new THREE.Object3D();
	graphics.title.line_2.symbol = [];
	for (let j = text[0][1].length - 1; j >= 0; j--) {
		const lineLength = new THREE.ShapeGeometry(Villula.generateShapes(text[0][1].slice(0, j + 1), 32.2), 1);
		lineLength.computeBoundingBox();
		graphics.title.line_2.symbol[j] = new THREE.Mesh(new THREE.ShapeGeometry(Villula.generateShapes(text[0][1].slice(j, j + 1), 32.2), 8), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
		graphics.title.line_2.symbol[j].geometry.computeBoundingBox();
		graphics.title.line_2.symbol[j].geometry.translate(lineLength.boundingBox.max.x - graphics.title.line_2.symbol[j].geometry.boundingBox.max.x, 0, 0);
    graphics.title.line_2.add(graphics.title.line_2.symbol[j]);
	}
	graphics.title.line_2.position.set(-98.5, -45, 0);
	
	tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(Villula.generateShapes(text[0][0], 32.2), 8);
  tempGeometry[tempGeometry.length - 1].translate(-98.5, 0, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(Villula.generateShapes(text[0][1], 32.2), 8);
	tempGeometry[tempGeometry.length - 1].translate(-98.5, -45, 0);

  
  const evaluator = new Evaluator();
  let brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry));
  brush1.updateMatrixWorld();
  let brush2 = new Brush(new THREE.BoxGeometry(600, 600, 600));
  brush2.geometry.translate(300, 0, 0);
  brush2.rotation.z = -0.5;
  brush2.position.y = -5;
  brush2.updateMatrixWorld();
  graphics.title.part_1 = new THREE.Mesh(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true }));
  
  graphics.title.part_1.geometry.translate(38, 5, 0);
  
  graphics.title.part_1.position.set(-38, -5, 0);
  
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(600, 600, 600));
  brush2.geometry.translate(-300, 0, 0);
  brush2.rotation.z = -0.5;
  brush2.position.y = -5;
  brush2.updateMatrixWorld();
  graphics.title.part_2 = new THREE.Mesh(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true }));
  graphics.title.part_2.geometry.translate(-38, 5, 0);
  graphics.title.part_2.position.set(38, -5, 0);


  clearTempGeometries();

  graphics.title.part_1.visible = false;
	graphics.title.part_2.visible = false;
	
	
	graphics.title.add(graphics.title.line_1, graphics.title.line_2, graphics.title.part_1, graphics.title.part_2);
  graphics.title.position.set(0, 109, -10);
  graphics.startText = new THREE.Object3D();
  graphics.startText.line = [];
  for (let j = 0; j < text[1].length; j++) {
    graphics.startText.line[j] = new THREE.Mesh(new THREE.ShapeGeometry(EuclidRegular.generateShapes(text[1][j], 10.78), 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
  	graphics.startText.line[j].geometry.computeBoundingBox();
  	graphics.startText.line[j].geometry.translate(-0.5 * graphics.startText.line[j].geometry.boundingBox.max.x, 0, 0);
  	graphics.startText.line[j].position.y = -20 * j;
  	if (j > 2) graphics.startText.line[j].position.y -= 20;
  	graphics.startText.add(graphics.startText.line[j]);
  }
	graphics.startText.position.set(0, 23, -10);
	graphics.startBubble = new THREE.Object3D();
	graphics.startBubble.bubble = new THREE.Mesh(new THREE.ShapeGeometry(svgPath[3][0].toShapes(true), 10), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  graphics.startBubble.bubble.geometry.translate(-184, 0, 0);
  graphics.startBubble.bubble.scale.set(1, -1, 1);
  graphics.startBubble.text = new THREE.Mesh(new THREE.ShapeGeometry(EuclidBold.generateShapes(`получи подарок`, 17.97), 6), new THREE.MeshBasicMaterial({ color: 0x066B53 }));
  graphics.startBubble.text.geometry.translate(-150, -54, 0.1);
  graphics.startBubble.add(graphics.startBubble.bubble, graphics.startBubble.text);
	graphics.startBubble.position.set(50, -90, -10);
	graphics.startBubble.scale.set(0, 0, 1);
	
	
	
	graphics.hintText_1 = new THREE.Object3D();
  graphics.hintText_1.line = [];
  for (let j = 0; j < text[2].length; j++) {
    graphics.hintText_1.line[j] = new THREE.Mesh(new THREE.ShapeGeometry(EuclidRegular.generateShapes(text[2][j], 10.78), 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
  	graphics.hintText_1.line[j].geometry.computeBoundingBox();
  	graphics.hintText_1.line[j].geometry.translate(-0.5 * graphics.hintText_1.line[j].geometry.boundingBox.max.x, 0, 0);
  	graphics.hintText_1.line[j].position.y = -20 * j;
  	
  	graphics.hintText_1.add(graphics.hintText_1.line[j]);
  }
  
  graphics.hintText_1.position.set(0, -100, -10);
  
    
  graphics.hintText_2 = new THREE.Object3D();
  graphics.hintText_2.line = [];
  for (let j = 0; j < text[3].length; j++) {
  	graphics.hintText_2.line[j] = new THREE.Mesh(new THREE.ShapeGeometry(EuclidRegular.generateShapes(text[3][j], 10.78), 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0 }));
  	graphics.hintText_2.line[j].geometry.computeBoundingBox();
  	graphics.hintText_2.line[j].geometry.translate(-0.5 * graphics.hintText_2.line[j].geometry.boundingBox.max.x, 0, 0);
  	graphics.hintText_2.line[j].position.y = -20 * j;
  	graphics.hintText_2.add(graphics.hintText_2.line[j]);
  }

  graphics.hintText_2.position.set(0, -120, -10);

	
	
	
	graphics.button = [];
	
	graphics.button[0] = new THREE.Object3D();
	let shape = new THREE.Shape();
	shape.absarc(149, 19, 11, 0, Math.PI * 0.5);
  shape.absarc(-149, 19, 11, Math.PI * 0.5, Math.PI);
  shape.absarc(-149, -19, 11, Math.PI, Math.PI * 1.5);
  shape.absarc(149, -19, 11, Math.PI * 1.5, Math.PI * 2);
  graphics.button[0].clickableArea = new THREE.Mesh(new THREE.ShapeGeometry(shape, 6), new THREE.MeshBasicMaterial({ color: 0xD7144B }));
  graphics.button[0].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 6), new THREE.MeshBasicMaterial({ color: 0x9F0F38 }));
  graphics.button[0].text = new THREE.Mesh(new THREE.ShapeGeometry(EuclidBold.generateShapes(`к столу`, 17.97), 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  graphics.button[0].text.geometry.center();
  graphics.button[0].back.position.set(0, -6, -0.1);
  graphics.button[0].text.position.set(0, -4, 0.1);
  graphics.button[0].add(graphics.button[0].clickableArea, graphics.button[0].back, graphics.button[0].text);
  graphics.button[0].position.set(0, -35, -10);
  graphics.button[0].ready = false;
  
  graphics.button[1] = new THREE.Object3D();
  shape = null;
	shape = new THREE.Shape();
	shape.absarc(149, 19, 11, 0, Math.PI * 0.5);
  shape.absarc(-149, 19, 11, Math.PI * 0.5, Math.PI);
  shape.absarc(-149, -19, 11, Math.PI, Math.PI * 1.5);
  shape.absarc(149, -19, 11, Math.PI * 1.5, Math.PI * 2);
  graphics.button[1].clickableArea = new THREE.Mesh(new THREE.ShapeGeometry(shape, 6), new THREE.MeshBasicMaterial({ color: 0xD7144B }));
  graphics.button[1].back = new THREE.Mesh(new THREE.ShapeGeometry(shape, 6), new THREE.MeshBasicMaterial({ color: 0x9F0F38 }));
  graphics.button[1].text = new THREE.Mesh(new THREE.ShapeGeometry(EuclidBold.generateShapes(`понятно`, 17.97), 6), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  graphics.button[1].text.geometry.center();
  graphics.button[1].back.position.set(0, -6, -0.1);
  graphics.button[1].text.position.set(0, -2, 0.1);
  graphics.button[1].add(graphics.button[1].clickableArea, graphics.button[1].back, graphics.button[1].text);
  graphics.button[1].position.set(0, -35, -10);
  graphics.button[1].ready = false;

  
  
  graphics.cutFX = [];
  graphics.cutSparkle = [];
  for(let i = 0; i < 10; i++) {
  	graphics.cutFX[i] = new THREE.Object3D();
  	graphics.cutFX[i].pic = new THREE.Mesh(new THREE.PlaneGeometry(tex[14].image.width * 0.3, tex[14].image.height * 0.3), new THREE.MeshBasicMaterial({ map: tex[14], transparent: true, alphaTest: 0.1 }))
    graphics.cutFX[i].back = new THREE.Mesh(new THREE.PlaneGeometry(tex[14].image.width * 0.3, tex[14].image.height * 0.3), new THREE.MeshBasicMaterial({ map: tex[15], transparent: true, alphaTest: 0.1 }))
    graphics.cutFX[i].add(graphics.cutFX[i].pic, graphics.cutFX[i].back);
    graphics.cutFX[i].on = false;
    
    graphics.cutSparkle[i] = new THREE.Object3D();
    graphics.cutSparkle[i].pic = [];
    for(let j = 0; j < 13; j++) {
    	graphics.cutSparkle[i].pic[j] = new THREE.Mesh(new THREE.PlaneGeometry(tex[j + 1].image.width * 0.33, tex[j + 1].image.height * 0.33), new THREE.MeshBasicMaterial({ map: tex[j + 1], transparent: true }))
      graphics.cutSparkle[i].add(graphics.cutSparkle[i].pic[j]);
    }
    graphics.cutSparkle[i].on = false;
  }
  
  graphics.scar = [];
  
  for(let i = 0; i < 5; i++) {
  	graphics.scar[i] = new THREE.Mesh(new THREE.PlaneGeometry(tex[20 + i].image.width * 0.33, tex[20 + i].image.height * 0.33), new THREE.MeshBasicMaterial({ map: tex[20 + i], transparent: true, opacity: 0 }))
    graphics.scar[i + 5] = new THREE.Mesh(new THREE.PlaneGeometry(tex[20 + i].image.width * 0.33, tex[20 + i].image.height * 0.33), new THREE.MeshBasicMaterial({ map: tex[20 + i], transparent: true, opacity: 0 }))
    graphics.scar[i].on = false;
    graphics.scar[i + 5].on = false;
  }
  
  graphics.blot = [];
  for (let i = 0; i < 4; i++) {
  	graphics.blot[i] = new THREE.Mesh(new THREE.PlaneGeometry(tex[16 + i].image.width * 0.33, tex[16 + i].image.height * 0.33), new THREE.MeshBasicMaterial({ map: tex[16 + i], transparent: true, opacity: 0 }))
  	graphics.blot[i + 4] = new THREE.Mesh(new THREE.PlaneGeometry(tex[16 + i].image.width * 0.33, tex[16 + i].image.height * 0.33), new THREE.MeshBasicMaterial({ map: tex[16 + i], transparent: true, opacity: 0 }))
  	graphics.blot[i + 8] = new THREE.Mesh(new THREE.PlaneGeometry(tex[16 + i].image.width * 0.33, tex[16 + i].image.height * 0.33), new THREE.MeshBasicMaterial({ map: tex[16 + i], transparent: true, opacity: 0 }))
  	graphics.blot[i].on = false;
  	graphics.blot[i + 4].on = false;
  	graphics.blot[i + 8].on = false;
  }
  
    
  graphics.backLineColor = [0x2DBE64, 0x57CB83, 0x6CD293, 0x81D8A2, 0x96DFB1, 0xABE5C1, 0xC1EBD0];
  graphics.backLine = [];
  for (let i = 0; i < 7; i++) {
    graphics.backLine[i] = new THREE.Mesh(new THREE.PlaneGeometry(4000, 4000), new THREE.MeshBasicMaterial({ color: graphics.backLineColor[i] }));
    if (i == 6) graphics.backLine[i].scale.x = 4
    graphics.backLine[i].rotation.z = Math.PI * 0.25;
    graphics.backLine[i].position.set(-1000, 500, -14990 - 0.1 * i);
    mainScene.add(graphics.backLine[i]);
  }
    
  graphics.table = new THREE.Mesh(new THREE.PlaneGeometry(tex[25].image.width * 0.32, tex[25].image.height * 0.32), new THREE.MeshBasicMaterial({ map: tex[25], transparent: true }))
  graphics.table.geometry.translate(0, tex[25].image.height * 0.16, 0);
  graphics.table.position.set(0, -140, -250);
  
  graphics.bowl = new THREE.Object3D()
  let path = new THREE.Path();
  path.absarc(-115, 62, 62, Math.PI, Math.PI * 1.5);
  
  graphics.bowlWhite = new THREE.Mesh(new THREE.LatheGeometry(path.getPoints(10), 56), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, side: THREE.BackSide }));
  
  
  path = null;
  path = new THREE.Path();
  path.absarc(-114.9, 62, 62, Math.PI, Math.PI * 1.5);
  path.absarc(-104.9, 0, 10, Math.PI, Math.PI * 1.5);
  path.lineTo(0, -10);
  graphics.bowlGray = new THREE.Mesh(new THREE.LatheGeometry(path.getPoints(10), 56), new THREE.MeshBasicMaterial({ color: 0xBDBDBD, side: THREE.DoubleSide }));

  graphics.bowl.add(graphics.bowlWhite, graphics.bowlGray);
  
  
  
  graphics.bowl.position.set(0, -82, -200);
  
  
   // 
// 320 60, 11
//	gsap.to(graphics.bowl.rotation, { duration: 5, x: Math.PI * 2, ease: "none", repeat: -1 });

	
	graphics.tempBack = new THREE.Mesh(new THREE.PlaneGeometry(360, 760), new THREE.MeshBasicMaterial({ map: tex[0], transparent: true, opacity: 0.3 }))
	
	graphics.tempBack.position.z = -20
	
//	mainScene.add(graphics.tempBack);
	
	
	
	graphics.productArea = [];
	graphics.productModule = [];
	graphics.productModule[0] = new THREE.Object3D();
	graphics.productModule[0].part = [new THREE.Object3D(), new THREE.Object3D()];
  path = null;
  path = new THREE.Path();
  path.absarc(40, 0, 45, Math.PI * 0.845, 0, true);
  path.absellipse(25, -3, 60, 110, Math.PI * 2, Math.PI * 1.365, true);
  graphics.productModule[0].part[0].outerBody = new THREE.Mesh(new THREE.LatheGeometry(path.getPoints(24), 24, 0, Math.PI), new THREE.MeshToonMaterial({ color: 0xD7144B, gradientMap: gradient, side: THREE.BackSide }));
  graphics.productModule[0].part[0].outerBody.geometry.rotateY(Math.PI * 0.5);
  graphics.productModule[0].part[0].outerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[0].part[0].outerBody.geometry);
  graphics.productModule[0].part[1].outerBody = new THREE.Mesh(new THREE.LatheGeometry(path.getPoints(24), 24, 0, Math.PI), new THREE.MeshToonMaterial({ color: 0xD7144B, gradientMap: gradient, side: THREE.BackSide }));
  graphics.productModule[0].part[1].outerBody.geometry.rotateY(-Math.PI * 0.5);
  graphics.productModule[0].part[1].outerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[0].part[1].outerBody.geometry);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(40, 0, 45, Math.PI * 0.845, 0, true);
  shape.absellipse(25, -3, 60, 110, Math.PI * 2, Math.PI * 1.365, true);
  shape.absellipse(-25, -3, 60, 110, Math.PI * 1.635, Math.PI, true);
  shape.absarc(-40, 0, 45, Math.PI, Math.PI * 0.155, true);
  graphics.productModule[0].part[0].innerBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 24), new THREE.MeshBasicMaterial({ color: 0xFFF2DB }));
  graphics.productModule[0].part[0].innerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[0].part[0].innerBody.geometry);
  graphics.productModule[0].part[1].innerBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 24), new THREE.MeshBasicMaterial({ color: 0xFFF2DB }));
  graphics.productModule[0].part[1].innerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[0].part[1].innerBody.geometry);
  graphics.productModule[0].part[1].innerBody.geometry.rotateY(Math.PI);
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 0, 6, 15, Math.PI * 0.9, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  tempGeometry[tempGeometry.length - 1].translate(10, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].translate(-20, 0, 0);
  graphics.productModule[0].part[0].seeds = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  graphics.productModule[0].part[0].seeds.geometry.translate(0, -5, 0.01);
  graphics.productModule[0].part[1].seeds = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  clearTempGeometries();
  graphics.productModule[0].part[1].seeds.geometry.rotateY(Math.PI);
  graphics.productModule[0].part[1].seeds.geometry.translate(0, -5, -1.5);
  graphics.productModule[0].stalk = new THREE.Mesh(new THREE.CylinderGeometry(3.5, 2.5, 60, 6, 1), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  graphics.productModule[0].stalk.geometry.translate(0, 75, -2.5);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 80, Math.PI * 2, Math.PI * 1.75, true);
  shape.lineTo(30, -15);
  shape.lineTo(40, 50);
  graphics.productModule[0].leaf = new THREE.Object3D();
  graphics.productModule[0].leafBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 8), new THREE.MeshBasicMaterial({ color: 0x2DBE64, side: THREE.DoubleSide }));
  graphics.productModule[0].leafBody.geometry.translate(-57, 0, 0);
  graphics.productModule[0].leafBody.geometry.rotateY(Math.PI * 0.35);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 80, Math.PI * 2, Math.PI * 1.75, true);
  shape.lineTo(67, -12);
  shape.lineTo(62, 22.5);
  graphics.productModule[0].leafShadow = new THREE.Mesh(new THREE.ShapeGeometry(shape, 8), new THREE.MeshBasicMaterial({ color: 0x333333, transparent: true, opacity: 0.3 }));
  graphics.productModule[0].leafShadow.geometry.translate(-57, 0, 0.01);
  graphics.productModule[0].leafShadow.geometry.rotateY(Math.PI * 0.35);
  graphics.productModule[0].leafShadow_2 = new THREE.Mesh(new THREE.ShapeGeometry(shape, 8), new THREE.MeshBasicMaterial({ color: 0x333333, transparent: true, opacity: 0.3, side: THREE.BackSide }));
  graphics.productModule[0].leafShadow_2.geometry.translate(-57, 0, -0.01);
  graphics.productModule[0].leafShadow_2.geometry.rotateY(Math.PI * 0.35);
  graphics.productModule[0].leaf.add(graphics.productModule[0].leafBody, graphics.productModule[0].leafShadow, graphics.productModule[0].leafShadow_2);
  graphics.productModule[0].leaf.rotation.z = 0.9;
  graphics.productModule[0].leaf.position.set(-43, 94, -2.5)
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(7, 5, 5, 0, Math.PI, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(Math.PI);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(7, 10);
  tempGeometry[tempGeometry.length - 1].translate(0, 0, -0.1);
  graphics.productModule[0].part[0].stalk = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  graphics.productModule[0].part[0].stalk.geometry.translate(0, -74, 0);
  graphics.productModule[0].part[1].stalk = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  clearTempGeometries();
  graphics.productModule[0].part[1].stalk.geometry.rotateY(Math.PI);
  graphics.productModule[0].part[1].stalk.geometry.translate(0, -74, 0.2);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], 90, 20, 0, 8, 0, 0, 80, 1.8);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -80, -20, 0, -7, 0, 0, 80, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -70, -80, 0, 5, 0, 0, 80, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -15, -110, 0, 0, 6, 0, 30, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -45, 45, 0, 0, 4, 0, 40, 1.2);
  graphics.productModule[0].part[0].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[1].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[0].innerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[1].innerBody.geometry.rotateY(Math.PI * 0.25);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], 15, -110, 0, 0, 6, 0, 30, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -30, 45, 0, 0, -6, 0, 40, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -75, -40, 0, -6, 0, 0, 80, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], 75, -60, 0, -4, 0, 0, 80, 0.5);
  graphics.productModule[0].part[0].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[1].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[0].innerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[1].innerBody.geometry.rotateY(Math.PI * 0.25);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -30, 45, 0, 0, 4, 0, 60, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -90, 20, 0, -8, 0, 0, 80, 1.8);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -70, -80, 0, 3, 0, 0, 80, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], 80, -60, 0, 3, 0, 0, 80, 2);
  graphics.productModule[0].part[0].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[1].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[0].innerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[0].part[1].innerBody.geometry.rotateY(Math.PI * 0.25);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], 80, -80, 0, -4, 0, 0, 80, 1);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], 90, 10, 0, 6, 0, 0, 80, 1.8);
  dragVertices([graphics.productModule[0].part[0].outerBody.geometry, graphics.productModule[0].part[1].outerBody.geometry, graphics.productModule[0].part[0].innerBody.geometry, graphics.productModule[0].part[1].innerBody.geometry], -80, -50, 0, -3, 0, 0, 80, 1.8);
  graphics.productModule[0].part[0].outerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[0].part[1].outerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[0].part[0].innerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[0].part[1].innerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[0].part[0].outerBody.geometry.translate(0, 25, 0);
  graphics.productModule[0].part[1].outerBody.geometry.translate(0, 25, 0);
  graphics.productModule[0].part[0].innerBody.geometry.translate(0, 25, 0);
  graphics.productModule[0].part[1].innerBody.geometry.translate(0, 25, 0);
  graphics.productModule[0].part[0].add(graphics.productModule[0].part[0].outerBody, graphics.productModule[0].part[0].innerBody, graphics.productModule[0].part[0].seeds, graphics.productModule[0].stalk, graphics.productModule[0].leaf, graphics.productModule[0].part[0].stalk);
	graphics.productModule[0].part[1].add(graphics.productModule[0].part[1].outerBody, graphics.productModule[0].part[1].innerBody, graphics.productModule[0].part[1].seeds, graphics.productModule[0].part[1].stalk);
	graphics.productModule[0].part[0].outerBody.position.z = 35;
  graphics.productModule[0].part[0].innerBody.position.z = 35;
  graphics.productModule[0].part[0].seeds.position.z = 35;
  graphics.productModule[0].stalk.position.z = 35;
  graphics.productModule[0].leaf.position.z = 35;
  graphics.productModule[0].part[0].stalk.position.z = 35;
  graphics.productModule[0].part[0].position.z = -35;
	graphics.productModule[0].part[0].container = new THREE.Object3D();
	graphics.productModule[0].part[0].container.add(graphics.productModule[0].part[0]);
	graphics.productModule[0].part[1].container = new THREE.Object3D();
  graphics.productModule[0].part[1].container.add(graphics.productModule[0].part[1]);
  graphics.productModule[0].part[1].outerBody.position.z = -35;
  graphics.productModule[0].part[1].innerBody.position.z = -35;
  graphics.productModule[0].part[1].seeds.position.z = -35;
  graphics.productModule[0].part[1].stalk.position.z = -35;
  graphics.productModule[0].part[1].position.z = 35;
	graphics.productModule[1] = new THREE.Object3D();
	graphics.productModule[1].part = [new THREE.Object3D(), new THREE.Object3D()];
  graphics.productModule[1].part[0].outerBody = new THREE.Mesh(new THREE.SphereGeometry(90, 36, 36, 0, Math.PI, 0, Math.PI), new THREE.MeshToonMaterial({ color: 0xFF6E00, gradientMap: gradient }));
  graphics.productModule[1].part[0].outerBody.geometry.rotateX(-Math.PI * 0.5);
  graphics.productModule[1].part[1].outerBody = new THREE.Mesh(new THREE.SphereGeometry(90, 36, 36, 0, Math.PI, 0, Math.PI), new THREE.MeshToonMaterial({ color: 0xFF6E00, gradientMap: gradient }));
  graphics.productModule[1].part[1].outerBody.geometry.rotateX(Math.PI * 0.5);
  graphics.productModule[1].part[0].innerBody = new THREE.Mesh(new THREE.CircleGeometry(90, 72), new THREE.MeshBasicMaterial({ color: 0xFFB369 }));
  graphics.productModule[1].part[0].innerBody.geometry.rotateX(Math.PI * 0.5);
  graphics.productModule[1].part[1].innerBody = new THREE.Mesh(new THREE.CircleGeometry(90, 72), new THREE.MeshBasicMaterial({ color: 0xFFB369 }));
  graphics.productModule[1].part[1].innerBody.geometry.rotateX(-Math.PI * 0.5);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, 0, Math.PI * 0.35);
  shape.lineTo(15, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, Math.PI * 0.38, Math.PI * 0.62);
  shape.lineTo(0, 17);
  shape.lineTo(12, 15);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, Math.PI * 0.65, Math.PI * 0.9);
  shape.lineTo(-10, 10);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, Math.PI * 0.93, Math.PI * 1.2);
  shape.lineTo(-12, 0);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, Math.PI * 1.23, Math.PI * 1.42);
  shape.lineTo(-5, -15);
  shape.lineTo(-12, -10);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, Math.PI * 1.45, Math.PI * 1.67);
  shape.lineTo(0, -15);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 82, Math.PI * 1.7, Math.PI * 1.97);
  shape.lineTo(12, -10);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 12);
  graphics.productModule[1].part[0].body = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFA5F0A }));
  graphics.productModule[1].part[0].body.geometry.rotateX(Math.PI * 0.5);
  graphics.productModule[1].part[0].body.geometry.translate(0, -0.8, 0);
  graphics.productModule[1].part[1].body = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFA5F0A }));
  clearTempGeometries();
  graphics.productModule[1].part[1].body.geometry.rotateX(-Math.PI * 0.5);
  graphics.productModule[1].part[1].body.geometry.translate(0, 0.8, 0);
  graphics.productModule[1].part[0].innerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[1].part[0].innerBody.geometry);
  graphics.productModule[1].part[1].innerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[1].part[1].innerBody.geometry);
  graphics.productModule[1].part[0].outerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[1].part[0].outerBody.geometry);
  graphics.productModule[1].part[1].outerBody.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[1].part[1].outerBody.geometry);
  graphics.productModule[1].part[0].body.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[1].part[0].body.geometry);
  graphics.productModule[1].part[1].body.geometry = new BufferGeometryUtils.mergeVertices(graphics.productModule[1].part[1].body.geometry);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 0, 90, 0, 0, -15, 0, 80, 1);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 60, 60, 0, 5, 0, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 85, -10, 0, 5, 0, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 30, -80, 0, 0, -5, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -60, -60, 0, -7, 0, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -80, 30, 0, -5, 5, 0, 80, 2);
  graphics.productModule[1].part[0].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[1].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[0].innerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[1].innerBody.geometry.rotateY(Math.PI * 0.25);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 80, 30, 0, 5, 5, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 70, -30, 0, 5, -5, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -80, 30, 0, -5, 0, 0, 60, 2);
  graphics.productModule[1].part[0].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[1].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[0].innerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[1].innerBody.geometry.rotateY(Math.PI * 0.25);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -40, 85, 0, 0, 5, 0, 60, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -60, -70, 0, -5, 0, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -80, -30, 0, 3, 0, 0, 60, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 80, 70, 0, 5, 0, 0, 60, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 85, -20, 0, 3, 0, 0, 60, 2);
  graphics.productModule[1].part[0].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[1].outerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[0].innerBody.geometry.rotateY(Math.PI * 0.25);
  graphics.productModule[1].part[1].innerBody.geometry.rotateY(Math.PI * 0.25);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], -90, -20, 0, -3, 0, 0, 60, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 70, 50, 0, -5, -5, 0, 80, 2);
  dragVertices([graphics.productModule[1].part[0].outerBody.geometry, graphics.productModule[1].part[1].outerBody.geometry, graphics.productModule[1].part[0].innerBody.geometry, graphics.productModule[1].part[1].innerBody.geometry, graphics.productModule[1].part[0].body.geometry, graphics.productModule[1].part[1].body.geometry], 70, -50, 0, -3, 3, 0, 80, 2);
  graphics.productModule[1].part[0].outerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[1].part[1].outerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[1].part[0].innerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[1].part[1].innerBody.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[1].stalk = new THREE.Mesh(new THREE.CylinderGeometry(2.8, 1.6, 46, 6, 1), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  graphics.productModule[1].stalk.geometry.translate(0, 95, 1.6);
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 90, 40, 90, Math.PI * 1.5, Math.PI * 2);
  shape.lineTo(37, 110);
  shape.lineTo(40, 160);
  shape.absarc(65, 55, 90, Math.PI * 0.85, Math.PI);
  shape.absarc(65, 55, 90, Math.PI * 1.1, Math.PI * 1.18);
  graphics.productModule[1].leaf = new THREE.Object3D();
  graphics.productModule[1].leafBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 8), new THREE.MeshBasicMaterial({ color: 0x18A019, side: THREE.DoubleSide }));
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 90, 40, 90, Math.PI * 1.5, Math.PI * 2);
  shape.lineTo(37, 110);
  shape.lineTo(40, 160);
  shape.lineTo(15, 85);
  shape.lineTo(0, 0);
  graphics.productModule[1].leafShadow = new THREE.Mesh(new THREE.ShapeGeometry(shape, 8), new THREE.MeshBasicMaterial({ color: 0x333333, transparent: true, opacity: 0.3 }));
  graphics.productModule[1].leafShadow.geometry.translate(0, 0, 0.01);
  graphics.productModule[1].leafShadow_2 = new THREE.Mesh(new THREE.ShapeGeometry(shape, 8), new THREE.MeshBasicMaterial({ color: 0x333333, transparent: true, opacity: 0.3, side: THREE.BackSide }));
  graphics.productModule[1].leafShadow_2.geometry.translate(0, 0, -0.01);
  graphics.productModule[1].leafStalk = new THREE.Mesh(new THREE.CylinderGeometry(1.5, 0.8, 40, 6, 2), new THREE.MeshBasicMaterial({ color: 0x333333 }));
  graphics.productModule[1].leafStalk.geometry.translate(0, 20, 0);
  graphics.productModule[1].leafStalk.geometry.rotateZ(Math.PI * 0.5);
  bendGeometry(graphics.productModule[1].leafStalk.geometry, "z", -0.06);
  graphics.productModule[1].leafStalk.geometry.rotateZ(0.8);
  graphics.productModule[1].leafStalk.geometry.rotateY(-Math.PI * 0.5);
  graphics.productModule[1].leafStalk.geometry.rotateX(-0.3);
  graphics.productModule[1].leaf.add(graphics.productModule[1].leafBody, graphics.productModule[1].leafShadow, graphics.productModule[1].leafShadow_2, graphics.productModule[1].leafStalk);
  graphics.productModule[1].leaf.position.set(17, 100, 28);
  graphics.productModule[1].leaf.rotation.set(-4, -0.3, -0.5);
  graphics.productModule[1].part[0].body.geometry.scale(0.98, 0.98, 1);
  graphics.productModule[1].part[1].body.geometry.scale(0.98, 0.98, 1);
  graphics.productModule[1].part[0].add(graphics.productModule[1].part[0].outerBody, graphics.productModule[1].part[0].innerBody, graphics.productModule[1].stalk, graphics.productModule[1].leaf, graphics.productModule[1].part[0].body);
  graphics.productModule[1].part[1].add(graphics.productModule[1].part[1].outerBody, graphics.productModule[1].part[1].innerBody, graphics.productModule[1].part[1].body);
  graphics.productModule[1].part[0].outerBody.position.y = -35;
  graphics.productModule[1].part[0].innerBody.position.y = -35;
  graphics.productModule[1].stalk.position.y = -35;
  graphics.productModule[1].leaf.position.y = 65;
  graphics.productModule[1].part[0].body.position.y = -35;
  graphics.productModule[1].part[0].position.y = 35;
  graphics.productModule[1].part[1].outerBody.position.y = 35;
  graphics.productModule[1].part[1].innerBody.position.y = 35;
  graphics.productModule[1].part[1].body.position.y = 35;
  graphics.productModule[1].part[1].position.y = -35;
  graphics.productModule[1].part[0].container = new THREE.Object3D();
	graphics.productModule[1].part[0].container.add(graphics.productModule[1].part[0]);
	graphics.productModule[1].part[1].container = new THREE.Object3D();
  graphics.productModule[1].part[1].container.add(graphics.productModule[1].part[1]);
  graphics.productModule[2] = new THREE.Object3D();
	graphics.productModule[2].part = [new THREE.Object3D(), new THREE.Object3D()];
  path = null;
  path = new THREE.Path();
  path.absellipse(65, 0, 25, 60, Math.PI * 1.5, Math.PI * 2.1);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(12), 48);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  path = null;
  path = new THREE.Path();
  path.absellipse(50, 0, 25, 50, Math.PI * 1.5, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 40);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(90, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absellipse(35, 0, 20, 30, Math.PI * 1.5, Math.PI * 1.8);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 36);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(159, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absarc(0, 0, 38, Math.PI * 1.5, Math.PI * 1.9);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(6), 24);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(199, 0, 0);
  graphics.productModule[2].part[0].outerBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFC9A48, gradientMap: gradient }));
  clearTempGeometries();
  path = null;
  path = new THREE.Path();
  path.absellipse(50, 0, 25, 50, Math.PI * 1.5, Math.PI * 2.2);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 40);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-90, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absellipse(35, 0, 20, 30, Math.PI * 2, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 36);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-159, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absarc(0, 0, 38, Math.PI * 1.9, Math.PI * 2.1);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(6), 24);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-199, 0, 0);
  graphics.productModule[2].part[1].outerBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFC9A48, gradientMap: gradient }));
  clearTempGeometries();
  path = null;
  path = new THREE.Path();
  path.absellipse(50, 0, 25, 50, Math.PI * 2, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 40);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(90, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absellipse(35, 0, 20, 30, Math.PI * 1.8, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 36);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(159, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absarc(0, 0, 38, Math.PI * 1.9, Math.PI * 2.1);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(6), 24);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(199, 0, 0);
  graphics.productModule[2].part[0].outerBody_dark = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xF98026, gradientMap: gradient }));
  clearTempGeometries();
  path = null;
  path = new THREE.Path();
  path.absellipse(50, 0, 25, 50, Math.PI * 2.2, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 40);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-90, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absellipse(35, 0, 20, 30, Math.PI * 1.5, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 36);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-159, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absarc(0, 0, 38, Math.PI * 1.5, Math.PI * 1.9);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(6), 24);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-199, 0, 0);
  path = null;
  path = new THREE.Path();
  path.absellipse(65, 0, 25, 60, Math.PI * 2.1, Math.PI * 2.5);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(8), 48);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  graphics.productModule[2].part[1].outerBody_dark = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xF98026, gradientMap: gradient }));
  clearTempGeometries();
  graphics.productModule[2].part[0].body = new THREE.Mesh(new THREE.CircleGeometry(65 + 25 * Math.cos(Math.PI * 0.1), 48), new THREE.MeshBasicMaterial({ color: 0xFEB368 }));
  graphics.productModule[2].part[0].body.geometry.rotateY(-Math.PI * 0.5);
  graphics.productModule[2].part[0].body.geometry.translate(-18, 0, 0);
  graphics.productModule[2].part[1].body = new THREE.Mesh(new THREE.CircleGeometry(65 + 25 * Math.cos(Math.PI * 0.1), 48), new THREE.MeshBasicMaterial({ color: 0xFEB368 }));
  graphics.productModule[2].part[1].body.geometry.rotateY(Math.PI * 0.5);
  graphics.productModule[2].part[1].body.geometry.translate(-19, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-10, 0, 30, Math.PI * 1.5, Math.PI * 2);
  shape.absarc(-70, 0, 90, Math.PI * 2, Math.PI * 2.25);
  shape.absellipse(-10, 18, 10, 48, Math.PI * 0.5, Math.PI * 1.5);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 10);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-19, 0, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(1.2);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.8, 0.8);
  tempGeometry[tempGeometry.length - 1].translate(0, -60, -20);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(-1.2);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.6, 0.25);
  tempGeometry[tempGeometry.length - 1].rotateX(-0.6);
  tempGeometry[tempGeometry.length - 1].translate(0, 55, 40);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(-1);
  tempGeometry[tempGeometry.length - 1].scale(1, 3, 0.4);
  tempGeometry[tempGeometry.length - 1].translate(0, -280, 40);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(210, 20, 240, Math.PI * 0.93, Math.PI);
  shape.absarc(-30, 68, 48, Math.PI * 1.5, Math.PI * 1.2, true);
  shape.absarc(0, 0, 80, Math.PI * 0.8, Math.PI * 0.6, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 10);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-19, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(210, 20, 240, Math.PI * 1.013, Math.PI * 1.07);
  shape.absarc(50, -177, 160, Math.PI * 0.653, Math.PI * 0.71);
  shape.absarc(35, -30, 100, Math.PI * 1.05, Math.PI * 0.85, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 10);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-19, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(35, -30, 109, Math.PI * 0.85, Math.PI * 0.98);
  shape.absarc(0, 0, 80, Math.PI, Math.PI * 0.88, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 10);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-19, 0, 0);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-50, 7, 80, Math.PI * 1.9, Math.PI * 2.3);
  shape.absarc(0, -7, 80, Math.PI * 0.5, Math.PI * 0.38, true);
  shape.absarc(-210, -110, 300, Math.PI * 0.2, Math.PI * 0.12, true);
  shape.absarc(47, -5, 25, Math.PI * 2, Math.PI * 1.2, true);
  tempGeometry[tempGeometry.length] = new THREE.ShapeGeometry(shape, 10);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-19, 0, 0);
  graphics.productModule[2].part[0].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFC9A48 }));
  graphics.productModule[2].part[1].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xFC9A48, side: THREE.BackSide }));
  clearTempGeometries();
  graphics.productModule[2].part[1].filling.geometry.translate(0.4, 0, 0);
  graphics.productModule[2].part[0].outerBody.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[0].outerBody_dark.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[0].body.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[0].filling.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[1].outerBody.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[1].outerBody_dark.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[1].body.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[1].filling.geometry.translate(0, 90, 0);
  graphics.productModule[2].part[0].outerBody.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[0].outerBody_dark.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[0].body.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[0].filling.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[1].outerBody.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[1].outerBody_dark.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[1].body.geometry.scale(0.6, 1.3, 1);
  graphics.productModule[2].part[1].filling.geometry.scale(0.6, 1.3, 1);
  bendGeometry(graphics.productModule[2].part[0].outerBody.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[0].outerBody_dark.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[0].body.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[0].filling.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[1].outerBody.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[1].outerBody_dark.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[1].body.geometry, "z", -0.01);
  bendGeometry(graphics.productModule[2].part[1].filling.geometry, "z", -0.01);
  graphics.productModule[2].part[0].outerBody.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[0].outerBody_dark.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[0].body.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[0].filling.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[1].outerBody.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[1].outerBody_dark.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[1].body.geometry.translate(0, -90, 0);
  graphics.productModule[2].part[1].filling.geometry.translate(0, -90, 0);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], 30, 120, 0, 0, 10, 0, 80, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], 130, 90, 0, 5, 10, 0, 60, 1.5);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], -130, 90, 0, -5, 10, 0, 60, 1.5);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], 190, -140, 0, -20, 10, 0, 60, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], -190, -140, 0, 20, 10, 0, 60, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], -30, -110, 0, 0, 8, 0, 80, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], -30, 0, 80, 0, 0, 10, 80, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], 30, 0, -80, 0, 0, -10, 80, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], 110, 0, 70, 0, 0, 11, 80, 2);
  dragVertices([graphics.productModule[2].part[0].outerBody.geometry, graphics.productModule[2].part[1].outerBody.geometry, graphics.productModule[2].part[0].outerBody_dark.geometry, graphics.productModule[2].part[1].outerBody_dark.geometry, graphics.productModule[2].part[0].body.geometry, graphics.productModule[2].part[1].body.geometry, graphics.productModule[2].part[0].filling.geometry, graphics.productModule[2].part[1].filling.geometry], -110, 0, -70, 0, 0, -11, 80, 2);
  graphics.productModule[2].part[0].add(graphics.productModule[2].part[0].outerBody, graphics.productModule[2].part[0].outerBody_dark, graphics.productModule[2].part[0].body, graphics.productModule[2].part[0].filling);
  graphics.productModule[2].part[1].add(graphics.productModule[2].part[1].outerBody, graphics.productModule[2].part[1].outerBody_dark, graphics.productModule[2].part[1].body, graphics.productModule[2].part[1].filling);
  graphics.productModule[2].part[0].outerBody.geometry.translate(-90, -20, 0);
  graphics.productModule[2].part[0].outerBody_dark.geometry.translate(-90, -20, 0);
  graphics.productModule[2].part[0].body.geometry.translate(-90, -20, 0);
  graphics.productModule[2].part[0].filling.geometry.translate(-90, -20, 0);
  graphics.productModule[2].part[1].outerBody.geometry.translate(90, -20, 0);
  graphics.productModule[2].part[1].outerBody_dark.geometry.translate(90, -20, 0);
  graphics.productModule[2].part[1].body.geometry.translate(90, -20, 0);
  graphics.productModule[2].part[1].filling.geometry.translate(90, -20, 0);
  graphics.productModule[2].part[0].position.x = 90;
  graphics.productModule[2].part[1].position.x = -90;
  graphics.productModule[2].part[0].container = new THREE.Object3D();
	graphics.productModule[2].part[0].container.add(graphics.productModule[2].part[0]);
	graphics.productModule[2].part[1].container = new THREE.Object3D();
  graphics.productModule[2].part[1].container.add(graphics.productModule[2].part[1]);
  graphics.productModule[3] = new THREE.Object3D();
	graphics.productModule[3].part = [new THREE.Object3D(), new THREE.Object3D()];
  path = null;
  path = new THREE.Path();
  path.absellipse(250, 0, 10, 90, Math.PI * 1.5, Math.PI * 2.5);
  path.lineTo(0, 90);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(24), 64, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(250, 64, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -90, 0);
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400));
  brush2.position.z = -200;
  brush2.updateMatrixWorld();
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400));
  brush2.geometry.translate(0, 0, -200);
  brush2.rotation.y = -Math.PI * 0.7;
  brush2.updateMatrixWorld();
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400));
  brush2.rotation.y = 0.6;
  brush2.position.z = 310;
  brush2.updateMatrixWorld();
  graphics.productArea[3] = new THREE.Mesh(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, transparentMaterial);
  graphics.productArea[3].geometry.rotateY(-Math.PI * 0.15);
  graphics.productArea[3].geometry.translate(160, 0, 0);
  graphics.productArea[3].geometry.scale(0.9, 0.9, 0.9);
  let tempEvaluateResult = evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry;
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(600, 1000, 600));
  brush2.geometry.translate(300, 0, 0);
  brush2.rotation.z = -0.4;
  brush2.rotation.y = 0.7;
  brush2.position.x = -140;
  brush2.updateMatrixWorld();
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshLambertMaterial({ color: 0xFFD508 }));
  brush1.updateMatrixWorld();
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 16, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 5, -20, 0, 0, -5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(15, 12, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 5, -20, 0, 5, 10, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -5, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(-60, 45, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 16, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 5, -20, 0, 0, -5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 20, -15, 0, 15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(30, 45, 150);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 16, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 5, -20, 0, 0, -5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -20, 15, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(60, -45, 100);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(20, 12, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -5, -20, 0, 0, 5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(100, 40, 80);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(10, 12, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -5, -20, 0, 0, 5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -50, 170);
  brush2 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFB369, gradientMap: gradient }));
  brush2.position.x = -140;
  brush2.updateMatrixWorld();
  clearTempGeometries();
  graphics.productModule[3].part[0].innerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(tempEvaluateResult);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(1000, 1000, 1000));
  brush2.geometry.translate(-500, 0, 0);
  brush2.rotation.z = -0.4;
  brush2.rotation.y = 0.7;
  brush2.position.x = -140;
  brush2.updateMatrixWorld();
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshLambertMaterial({ color: 0xFFD508 }));
  brush1.updateMatrixWorld();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 16, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 5, -20, 0, 0, -5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(25, 12, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -5, -20, 0, 0, 5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(80, -60, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(10, 12, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -5, -20, 0, 0, 5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(65, 40, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 12, 24);
  tempGeometry[tempGeometry.length - 1].translate(140, 40, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(20, 12, 24);
  tempGeometry[tempGeometry.length - 1].translate(130, -30, 10);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 16, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 5, -20, 0, 0, -5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -20, 15, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(60, -45, 100);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(20, 12, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -5, -20, 0, 0, 5, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 20, 20, 0, -15, 0, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].translate(100, 40, 80);
  brush2 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFB369, gradientMap: gradient }));
  brush2.position.x = -140;
  brush2.updateMatrixWorld();
  graphics.productModule[3].part[1].innerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  clearTempGeometries();
  path = null;
  path = new THREE.Path();
  path.absellipse(250, 0, 9.9, 89.9, Math.PI * 1.5, Math.PI * 2.5);
  path.lineTo(0, 89.9);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(24), 64);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(250, 64);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -89.9, 0);
  brush1 = new Brush(graphics.productModule[3].part[0].innerBody.geometry.clone());
  brush1.updateMatrixWorld();
  brush2 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry));
  brush2.updateMatrixWorld();
  graphics.productModule[3].part[0].outerBody = new THREE.Mesh(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshBasicMaterial({ color: 0xFFB369 }));
  brush1 = new Brush(graphics.productModule[3].part[1].innerBody.geometry.clone());
  brush1.updateMatrixWorld();
  brush2 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry));
  brush2.updateMatrixWorld();
  graphics.productModule[3].part[1].outerBody = new THREE.Mesh(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshBasicMaterial({ color: 0xFFB369 }));
  clearTempGeometries();
  graphics.productModule[3].part[0].add(graphics.productModule[3].part[0].innerBody, graphics.productModule[3].part[0].outerBody);
  graphics.productModule[3].part[1].add(graphics.productModule[3].part[1].innerBody, graphics.productModule[3].part[1].outerBody);
  graphics.productModule[3].part[0].innerBody.geometry.rotateY(-Math.PI * 0.15);
  graphics.productModule[3].part[0].outerBody.geometry.rotateY(-Math.PI * 0.15);
  graphics.productModule[3].part[1].innerBody.geometry.rotateY(-Math.PI * 0.15);
  graphics.productModule[3].part[1].outerBody.geometry.rotateY(-Math.PI * 0.15);
  graphics.productModule[3].part[0].innerBody.position.set(160, 0, 0);
  graphics.productModule[3].part[0].outerBody.position.set(160, 0, 0);
  graphics.productModule[3].part[1].innerBody.position.set(80, 0, 0);
  graphics.productModule[3].part[1].outerBody.position.set(80, 0, 0);
  graphics.productModule[3].part[1].position.x = 80;
  graphics.productModule[3].part[0].container = new THREE.Object3D();
	graphics.productModule[3].part[0].container.add(graphics.productModule[3].part[0]);
	graphics.productModule[3].part[1].container = new THREE.Object3D();
  graphics.productModule[3].part[1].container.add(graphics.productModule[3].part[1]);
  graphics.productModule[4] = new THREE.Object3D();
	graphics.productModule[4].part = [new THREE.Object3D(), new THREE.Object3D()];
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(60, 60, 160, 48, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, -80, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(60, 48, 20, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.5, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -160, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(3, 10, 20, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, -200, 0);
  graphics.productModule[4].part[0].outerBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xF5675A, gradientMap: gradient }));
  clearTempGeometries();
  dragVertices([graphics.productModule[4].part[0].outerBody.geometry], 0, -160, -60, 0, -20, 0, 95, 2);
  dragVertices([graphics.productModule[4].part[0].outerBody.geometry], 0, -160, 60, 0, -5, 0, 95, 2);
  dragVertices([graphics.productModule[4].part[0].outerBody.geometry], 0, -160, 60, 0, -10, 0, 95, 2);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(60, 60, 100, 48, 1, true);
  tempGeometry[tempGeometry.length - 1].translate(0, 50, 0);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(60, 48, 20, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.5, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 100, 0);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(10, 3, 20, 16, 1);
  tempGeometry[tempGeometry.length - 1].translate(0, 140, 0);
  graphics.productModule[4].part[1].outerBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xF5675A, gradientMap: gradient }));
  clearTempGeometries();
  dragVertices([graphics.productModule[4].part[1].outerBody.geometry], -60, 100, 0, 0, 20, 0, 95, 2);
  dragVertices([graphics.productModule[4].part[1].outerBody.geometry], 0, 100, 60, 0, 5, 0, 95, 2);
  dragVertices([graphics.productModule[4].part[1].outerBody.geometry], 60, 100, 0, 0, 10, 0, 95, 2);
  graphics.productModule[4].part[0].innerBody = new THREE.Mesh(new THREE.CircleGeometry(60, 48), new THREE.MeshBasicMaterial({ color: 0xFA8B81 }));
  graphics.productModule[4].part[0].innerBody.geometry.rotateX(-Math.PI * 0.5);
  graphics.productModule[4].part[1].innerBody = new THREE.Mesh(new THREE.CircleGeometry(60, 48), new THREE.MeshBasicMaterial({ color: 0xFA8B81 }));
  graphics.productModule[4].part[1].innerBody.geometry.rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(20, 24);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -25, 25, 0, 10, 20, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].rotateX(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(20, 0.1, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].scale(0.7, 1, 0.7);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.7);
  tempGeometry[tempGeometry.length - 1].translate(-0, 0, 20);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].scale(0.8, 1, 0.8);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.6);
  graphics.productModule[4].part[0].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xF5675A }));
  graphics.productModule[4].part[1].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0xF5675A, side: THREE.BackSide }));
  clearTempGeometries();
  graphics.productModule[4].part[1].filling.geometry.translate(0, -0.2, 0);
  graphics.productModule[4].part[0].add(graphics.productModule[4].part[0].outerBody, graphics.productModule[4].part[0].innerBody, graphics.productModule[4].part[0].filling);
  graphics.productModule[4].part[1].add(graphics.productModule[4].part[1].outerBody, graphics.productModule[4].part[1].innerBody, graphics.productModule[4].part[1].filling);
  graphics.productModule[4].part[0].outerBody.position.y = 80;
  graphics.productModule[4].part[0].innerBody.position.y = 80;
  graphics.productModule[4].part[0].filling.position.y = 80;
  graphics.productModule[4].part[0].position.y = -50;
  graphics.productModule[4].part[1].outerBody.position.y = -80;
  graphics.productModule[4].part[1].innerBody.position.y = -80;
  graphics.productModule[4].part[1].filling.position.y = -80;
  graphics.productModule[4].part[1].position.y = 110;
  graphics.productModule[4].part[0].container = new THREE.Object3D();
	graphics.productModule[4].part[0].container.add(graphics.productModule[4].part[0]);
	graphics.productModule[4].part[1].container = new THREE.Object3D();
  graphics.productModule[4].part[1].container.add(graphics.productModule[4].part[1]);
  graphics.productModule[5] = new THREE.Object3D();
	graphics.productModule[5].part = [new THREE.Object3D(), new THREE.Object3D()];
  path = null;
  path = new THREE.Path();
  path.moveTo(250, -100);
  path.lineTo(250, 100);
  tempGeometry[tempGeometry.length] = new THREE.LatheGeometry(path.getPoints(24), 64, 0);
  tempGeometry[tempGeometry.length] = new THREE.CircleGeometry(250, 64, 0, Math.PI * 2);
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, -100, 0);
  tempGeometry[tempGeometry.length] = tempGeometry[tempGeometry.length - 1].clone();
  tempGeometry[tempGeometry.length - 1].rotateX(Math.PI);
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400));
  brush2.position.z = -200;
  brush2.updateMatrixWorld();
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400));
  brush2.geometry.translate(0, 0, -200);
  brush2.rotation.y = -Math.PI * 0.75;
  brush2.updateMatrixWorld();
  brush1 = new Brush(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 700), new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush2.geometry.translate(0, -150, 0);
  brush2.position.y = -60;
  brush2.rotation.x = -Math.PI * 0.02;
  brush2.rotation.z = Math.PI * 0.05;
  brush2.updateMatrixWorld();
  graphics.productArea[5] = new THREE.Mesh(evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry, transparentMaterial);
  graphics.productArea[5].geometry.rotateY(-Math.PI * 0.125);
  graphics.productArea[5].geometry.translate(160, 0, 0);
  graphics.productArea[5].geometry.scale(0.9, 0.9, 0.9);
  graphics.productModule[5].part[0].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  clearTempGeometries();
  brush1 = new Brush(graphics.productModule[5].part[0].outerBody.geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush2.geometry.translate(-350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[1].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.productModule[5].part[0].outerBody.geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush2.geometry.translate(350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].outerBody.geometry.dispose();
  graphics.productModule[5].part[0].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  tempGeometry[tempGeometry.length] = new THREE.PlaneGeometry(250.3, 80, 50, 10);
  tempGeometry[tempGeometry.length - 1].translate(125.15, 0, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 220, -50, 0, 0, -40, 0, 30, 1.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 150, -50, 0, 0, -60, 0, 40, 1.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 100, -50, 0, 0, -30, 0, 20, 1.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 60, -50, 0, 0, -40, 0, 30, 1.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 20, -50, 0, 0, -20, 0, 25, 1.5);
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400));
  brush2.position.y = 115;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].top_1 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  graphics.productModule[5].part[0].top_1.geometry.translate(0, 35, 0);
  graphics.productModule[5].part[0].top_1.geometry.scale(1, 2, 1);
  graphics.productModule[5].part[0].top_1.geometry.rotateY(Math.PI);
  graphics.productModule[5].part[0].top_1.geometry.translate(0.1, 100.1, -0.1);
  clearTempGeometries();
  dragVertices([graphics.productModule[5].part[0].top_1.geometry], 230, -25, 0, 5, -10, 0, 30, 2);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry], 185, -50, 0, 0, -50, 0, 40, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry], 115, -50, 0, 5, -30, 0, 45, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry], 50, -35, 0, -5, 0, 0, 30, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry], 20, -30, 0, -5, 0, 0, 30, 1);
  brush1 = new Brush(graphics.productModule[5].part[0].top_1.geometry, new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(-350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[1].top_1 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  dragVertices([graphics.productModule[5].part[1].top_1.geometry], 230, -25, 0, 5, -10, 0, 30, 2);
  dragVertices([graphics.productModule[5].part[1].top_1.geometry], 185, -50, 0, 0, -50, 0, 40, 1);
  dragVertices([graphics.productModule[5].part[1].top_1.geometry], 115, -50, 0, 5, -30, 0, 45, 1);
  dragVertices([graphics.productModule[5].part[1].top_1.geometry], 50, -35, 0, -5, 0, 0, 30, 1);
  dragVertices([graphics.productModule[5].part[1].top_1.geometry], 20, -30, 0, -5, 0, 0, 30, 1);
  graphics.productModule[5].part[0].top_2 = new THREE.Mesh(graphics.productModule[5].part[0].top_1.geometry.clone(), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }))
  graphics.productModule[5].part[0].top_2.geometry.translate(-0.1, 0, 0.1);
  graphics.productModule[5].part[0].top_2.geometry.translate(250.3, 0, 0);
  graphics.productModule[5].part[0].top_2.geometry.rotateY(Math.PI * 1.25);
  graphics.productModule[5].part[0].top_2.geometry.translate(0.1, 0, 0.1);
  brush1 = new Brush(graphics.productModule[5].part[0].top_2.geometry, new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(-350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[1].top_2 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  tempGeometry[tempGeometry.length] = new THREE.CylinderGeometry(250.3, 250.3, 80, 40, 10, true, 0, Math.PI * 0.25);
  tempGeometry[tempGeometry.length - 1].rotateY(-Math.PI * 0.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -245, -50, 30, 0, -50, 0, 30, 1.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -230, -50, 100, 0, -80, 0, 40, 1.5);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -200, -50, 160, 0, -60, 0, 30, 1.5);
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(1000, 300, 1000));
  brush2.position.y = 115;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].top_3 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  graphics.productModule[5].part[0].top_3.geometry.translate(0, 35, 0);
  graphics.productModule[5].part[0].top_3.geometry.scale(1, 2, 1);
  graphics.productModule[5].part[0].top_3.geometry.translate(0.1, 100.1, -0.1);
  brush1 = new Brush(graphics.productModule[5].part[0].top_3.geometry, new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(-350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[1].top_3 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(0, 0, 250.3, 0, Math.PI * 0.25);
  shape.lineTo(0, 0);
  graphics.productModule[5].part[0].top_4 = new THREE.Mesh(new THREE.ShapeGeometry(shape, 40), new THREE.MeshBasicMaterial({ color: 0xA04975 }));
  graphics.productModule[5].part[0].top_4.geometry.rotateX(-Math.PI * 0.5);
  graphics.productModule[5].part[0].top_4.geometry.rotateY(Math.PI);
  graphics.productModule[5].part[0].top_4.geometry.translate(0.1, 100.1, -0.1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry, graphics.productModule[5].part[0].top_2.geometry, graphics.productModule[5].part[0].top_3.geometry, graphics.productModule[5].part[1].top_1.geometry, graphics.productModule[5].part[1].top_2.geometry, graphics.productModule[5].part[1].top_3.geometry], -60, 110, 0, 0, 10, 0, 60, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry, graphics.productModule[5].part[0].top_2.geometry, graphics.productModule[5].part[0].top_3.geometry, graphics.productModule[5].part[1].top_1.geometry, graphics.productModule[5].part[1].top_2.geometry, graphics.productModule[5].part[1].top_3.geometry], -195, 110, 0, 0, 12, 0, 55, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry, graphics.productModule[5].part[0].top_2.geometry, graphics.productModule[5].part[0].top_3.geometry, graphics.productModule[5].part[1].top_1.geometry, graphics.productModule[5].part[1].top_2.geometry, graphics.productModule[5].part[1].top_3.geometry], -230, 110, 80, 0, 15, 0, 80, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry, graphics.productModule[5].part[0].top_2.geometry, graphics.productModule[5].part[0].top_3.geometry, graphics.productModule[5].part[1].top_1.geometry, graphics.productModule[5].part[1].top_2.geometry, graphics.productModule[5].part[1].top_3.geometry], -90, 110, 60, 0, 10, 0, 60, 1);
  dragVertices([graphics.productModule[5].part[0].top_1.geometry, graphics.productModule[5].part[0].top_2.geometry, graphics.productModule[5].part[0].top_3.geometry, graphics.productModule[5].part[1].top_1.geometry, graphics.productModule[5].part[1].top_2.geometry, graphics.productModule[5].part[1].top_3.geometry], -175, 110, 140, 0, 15, 0, 35, 1);
  brush1 = new Brush(graphics.productModule[5].part[0].top_4.geometry, new THREE.MeshBasicMaterial({ color: 0xA04975 }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(-350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[1].top_4 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.productModule[5].part[0].top_1.geometry, new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].top_1.geometry.dispose();
  graphics.productModule[5].part[0].top_1 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.productModule[5].part[0].top_2.geometry, new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].top_2.geometry.dispose();
  graphics.productModule[5].part[0].top_2 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.productModule[5].part[0].top_3.geometry, new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].top_3.geometry.dispose();
  graphics.productModule[5].part[0].top_3 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.productModule[5].part[0].top_4.geometry, new THREE.MeshBasicMaterial({ color: 0xA04975 }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(700, 300, 400), new THREE.MeshBasicMaterial({ color: 0x7e3a5c }));
  brush2.geometry.translate(350, 0, 0);
  brush2.position.x = -130;
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].top_4.geometry.dispose();
  graphics.productModule[5].part[0].top_4 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  clearTempGeometries();
  brush1 = new Brush(graphics.productModule[5].part[0].outerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(190, 50, 400));
  brush2.geometry.translate(-95, 0, 0);
  brush2.position.set(-20, 50, 0);
  brush2.rotation.y = Math.PI * 0.125;
  brush2.updateMatrixWorld();
  tempGeometry[tempGeometry.length] = evaluator.evaluate(brush1, brush2, INTERSECTION).geometry;
  brush1 = new Brush(graphics.productModule[5].part[0].outerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(190, 50, 400));
  brush2.geometry.translate(-95, 0, 0);
  brush2.position.set(-20, -20, 0);
  brush2.rotation.y = Math.PI * 0.125;
  brush2.rotation.x = Math.PI * -0.01;
  brush2.rotation.z = Math.PI * 0.015;
  brush2.updateMatrixWorld();
  tempGeometry[tempGeometry.length] = evaluator.evaluate(brush1, brush2, INTERSECTION).geometry;
  graphics.productModule[5].part[0].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xFFD5AB }));
  clearTempGeometries();
  brush1 = new Brush(graphics.productModule[5].part[0].outerBody.geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(graphics.productModule[5].part[0].filling.geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[0].outerBody.geometry.dispose();
  graphics.productModule[5].part[0].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.productModule[5].part[1].outerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(240, 50, 400));
  brush2.geometry.translate(-120, 0, 0);
  brush2.position.set(-20, 50, 0);
  brush2.rotation.y = Math.PI * 0.125;
  brush2.updateMatrixWorld();
  tempGeometry[tempGeometry.length] = evaluator.evaluate(brush1, brush2, INTERSECTION).geometry;
  brush1 = new Brush(graphics.productModule[5].part[1].outerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.BoxGeometry(190, 50, 400));
  brush2.geometry.translate(-95, 0, 0);
  brush2.position.set(-20, -20, 0);
  brush2.rotation.y = Math.PI * 0.125;
  brush2.rotation.x = Math.PI * -0.01;
  brush2.rotation.z = Math.PI * 0.015;
  brush2.updateMatrixWorld();
  tempGeometry[tempGeometry.length] = evaluator.evaluate(brush1, brush2, INTERSECTION).geometry;
  graphics.productModule[5].part[1].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshLambertMaterial({ color: 0xFFD5AB }));
  clearTempGeometries();
  brush1 = new Brush(graphics.productModule[5].part[1].outerBody.geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(graphics.productModule[5].part[1].filling.geometry, new THREE.MeshLambertMaterial({ color: 0x73331E }));
  brush2.updateMatrixWorld();
  graphics.productModule[5].part[1].outerBody.geometry.dispose();
  graphics.productModule[5].part[1].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 32, 16);
  tempGeometry[tempGeometry.length - 1].translate(-160, 120, 30);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(25, 32, 16);
  tempGeometry[tempGeometry.length - 1].translate(-180, 115, 130);
  graphics.productModule[5].part[0].top_5 = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFA5F05, gradientMap: gradient }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(8, 24, 6, 12);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(-0.5);
  tempGeometry[tempGeometry.length - 1].translate(-210, 108, 60);
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(8, 24, 6, 12);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(0.8);
  tempGeometry[tempGeometry.length - 1].translate(-140, 108, 90);
  graphics.productModule[5].part[0].top_6 = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFC9A4B, gradientMap: gradient }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.CapsuleGeometry(8, 24, 6, 12);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateY(-0.3);
  tempGeometry[tempGeometry.length - 1].translate(-90, 108, 40);
  graphics.productModule[5].part[1].top_5 = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFC9A4B, gradientMap: gradient }));
  clearTempGeometries();
  graphics.productModule[5].part[0].add(graphics.productModule[5].part[0].outerBody, graphics.productModule[5].part[0].top_1, graphics.productModule[5].part[0].top_2, graphics.productModule[5].part[0].top_3, graphics.productModule[5].part[0].top_4, graphics.productModule[5].part[0].filling, graphics.productModule[5].part[0].top_5, graphics.productModule[5].part[0].top_6);
  graphics.productModule[5].part[1].add(graphics.productModule[5].part[1].outerBody, graphics.productModule[5].part[1].top_1, graphics.productModule[5].part[1].top_2, graphics.productModule[5].part[1].top_3, graphics.productModule[5].part[1].top_4, graphics.productModule[5].part[1].filling, graphics.productModule[5].part[1].top_5);
  graphics.productModule[5].part[0].outerBody.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].top_1.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].top_2.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].top_3.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].top_4.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].filling.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].top_5.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].top_6.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].outerBody.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].top_1.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].top_2.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].top_3.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].top_4.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].filling.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[1].top_5.geometry.rotateY(-Math.PI * 0.125);
  graphics.productModule[5].part[0].outerBody.position.set(160, 0, 0);
  graphics.productModule[5].part[0].top_1.position.set(160, 0, 0);
  graphics.productModule[5].part[0].top_2.position.set(160, 0, 0);
  graphics.productModule[5].part[0].top_3.position.set(160, 0, 0);
  graphics.productModule[5].part[0].top_4.position.set(160, 0, 0);
  graphics.productModule[5].part[0].filling.position.set(160, 0, 0);
  graphics.productModule[5].part[0].top_5.position.set(160, 0, 0);
  graphics.productModule[5].part[0].top_6.position.set(160, 0, 0);
  graphics.productModule[5].part[1].outerBody.position.set(80, 0, 0);
  graphics.productModule[5].part[1].top_1.position.set(80, 0, 0);
  graphics.productModule[5].part[1].top_2.position.set(80, 0, 0);
  graphics.productModule[5].part[1].top_3.position.set(80, 0, 0);
  graphics.productModule[5].part[1].top_4.position.set(80, 0, 0);
  graphics.productModule[5].part[1].filling.position.set(80, 0, 0);
  graphics.productModule[5].part[1].top_5.position.set(80, 0, 0);
  graphics.productModule[5].part[1].position.x = 80;
  graphics.productModule[5].part[0].container = new THREE.Object3D();
	graphics.productModule[5].part[0].container.add(graphics.productModule[5].part[0]);
	graphics.productModule[5].part[1].container = new THREE.Object3D();
  graphics.productModule[5].part[1].container.add(graphics.productModule[5].part[1]);
  graphics.productModule[6] = new THREE.Object3D();
	graphics.productModule[6].part = [new THREE.Object3D(), new THREE.Object3D()];
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(45, 20, 16, 0, Math.PI * 2, 0, Math.PI * 0.43);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5 + 0.11);
  tempGeometry[tempGeometry.length - 1].translate(-27, 180, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -27, 230, 0, 12, 10, 0, 55, 2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(40, 20, 16);
  tempGeometry[tempGeometry.length - 1].translate(-95, 160, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -95, 205, 0, 0, 10, 0, 55, 2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(35, 20, 16);
  tempGeometry[tempGeometry.length - 1].translate(-140, 120, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -150, 160, 0, -20, 0, 0, 55, 2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(30, 20, 16);
  tempGeometry[tempGeometry.length - 1].translate(-160, 70, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -195, 70, 0, -5, -5, 0, 40, 2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(25, 20, 16);
  tempGeometry[tempGeometry.length - 1].scale(2, 1.2, 1);
  tempGeometry[tempGeometry.length - 1].translate(-140, 35, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -200, 30, 0, 5, -30, 0, 100, 2);
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient }));
  brush1.geometry.scale(1, 1, 0.5);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.SphereGeometry(90, 48, 24, 0, Math.PI * 2, 0, Math.PI * 0.55), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient }));
  brush2.geometry.rotateZ(Math.PI * 0.5);
  brush2.geometry.translate(0, 90, 0);
  brush2.geometry.scale(1, 1, 0.7);
  bendGeometry(brush2.geometry, "z", -0.009);
  brush2.updateMatrixWorld();
  graphics.productModule[6].part[0].top = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  graphics.productModule[6].part[0].top_1 = new THREE.Mesh(new THREE.CircleGeometry(45 * Math.cos(Math.PI * 0.07), 32));
  graphics.productModule[6].part[0].top_1.geometry.rotateX(Math.PI * 0.5);
  graphics.productModule[6].part[0].top_1.geometry.translate(0, 45 * Math.sin(Math.PI * 0.07), 0);
  graphics.productModule[6].part[0].top_1.geometry.rotateZ(Math.PI * 0.5 + 0.11);
  graphics.productModule[6].part[0].top_1.geometry.translate(-27, 180, 0);
  dragVertices([graphics.productModule[6].part[0].top_1.geometry], -27, 230, 0, 12, 10, 0, 55, 2);
  brush1 = new Brush(graphics.productModule[6].part[0].top_1.geometry, new THREE.MeshLambertMaterial({ color: 0xE7D8C6 }));
  brush1.geometry.scale(1, 1, 0.5);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.SphereGeometry(90, 48, 24, 0, Math.PI * 2, 0, Math.PI * 0.55), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient }));
  brush2.geometry.rotateZ(Math.PI * 0.5);
  brush2.geometry.translate(0, 90, 0);
  brush2.geometry.scale(1, 1, 0.7);
  bendGeometry(brush2.geometry, "z", -0.009);
  brush2.updateMatrixWorld();
  graphics.productModule[6].part[0].top_1.geometry.dispose();
  graphics.productModule[6].part[0].top_1 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(45, 20, 16, 0, Math.PI * 2, 0, Math.PI * 0.57);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5 + 0.11);
  tempGeometry[tempGeometry.length - 1].translate(-27, 180, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -27, 230, 0, 12, 10, 0, 55, 2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(50, 20, 16);
  tempGeometry[tempGeometry.length - 1].translate(50, 170, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 70, 225, 0, 10, 5, 0, 55, 2);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(50, 20, 16);
  tempGeometry[tempGeometry.length - 1].translate(115, 135, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 115, 190, 0, 10, 5, 0, 55, 2);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 170, 135, 0, 10, 0, 0, 55, 1);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(40, 20, 16);
  tempGeometry[tempGeometry.length - 1].translate(160, 80, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 205, 80, 0, 10, 0, 0, 55, 1);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(25, 20, 16);
  tempGeometry[tempGeometry.length - 1].scale(2.4, 1.2, 1.4);
  tempGeometry[tempGeometry.length - 1].translate(140, 45, 0);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 200, 35, 0, 5, -30, 0, 100, 2);
  brush1 = new Brush(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient }));
  brush1.geometry.scale(1, 1, 0.5);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.SphereGeometry(90, 48, 24, 0, Math.PI * 2, 0, Math.PI * 0.6), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient }));
  brush2.geometry.rotateZ(-Math.PI * 0.5);
  brush2.geometry.translate(0, 90, 0);
  brush2.geometry.scale(1, 1, 0.7);
  bendGeometry(brush2.geometry, "z", -0.009);
  brush2.updateMatrixWorld();
  clearTempGeometries();
  graphics.productModule[6].part[1].top = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  graphics.productModule[6].part[1].top_1 = new THREE.Mesh(new THREE.CircleGeometry(45 * Math.cos(Math.PI * 0.07), 32));
  graphics.productModule[6].part[1].top_1.geometry.rotateX(Math.PI * 0.5);
  graphics.productModule[6].part[1].top_1.geometry.translate(0, -45 * Math.sin(Math.PI * 0.07), 0);
  graphics.productModule[6].part[1].top_1.geometry.rotateZ(-Math.PI * 0.5 + 0.11);
  graphics.productModule[6].part[1].top_1.geometry.translate(-27, 180, 0);
  dragVertices([graphics.productModule[6].part[1].top_1.geometry], -27, 230, 0, 12, 10, 0, 55, 2);
  brush1 = new Brush(graphics.productModule[6].part[1].top_1.geometry, new THREE.MeshLambertMaterial({ color: 0xE7D8C6 }));
  brush1.geometry.scale(1, 1, 0.5);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.SphereGeometry(90, 48, 24, 0, Math.PI * 2, 0, Math.PI * 0.6));
  brush2.geometry.rotateZ(-Math.PI * 0.5);
  brush2.geometry.translate(0, 90, 0);
  brush2.geometry.scale(1, 1, 0.7);
  bendGeometry(brush2.geometry, "z", -0.009);
  brush2.updateMatrixWorld();
  graphics.productModule[6].part[1].top_1.geometry.dispose();
  graphics.productModule[6].part[1].top_1 = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(90, 48, 24, 0, Math.PI * 2, 0, Math.PI * 0.45);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 90, 0);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.7);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -0.009);
  graphics.productModule[6].part[0].outerBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient, side: THREE.DoubleSide }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(90, 48, 24, 0, Math.PI * 2, 0, Math.PI * 0.55);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(0, 90, 0);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.7);
  bendGeometry(tempGeometry[tempGeometry.length - 1], "z", -0.009);
  graphics.productModule[6].part[1].outerBody = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xE7D8C6, gradientMap: gradient, side: THREE.DoubleSide }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(140, 36, 10, 0, Math.PI * 2, 0, Math.PI * 0.21);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.7);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -60, 140, 40, 0, 30, 0, 40, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 40, 145, -30, 0, 20, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 30, 145, 20, 0, 20, 0, 40, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -30, 145, -20, 0, 20, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5 + 0.11);
  tempGeometry[tempGeometry.length - 1].translate(-150, 75, 0);
  graphics.productModule[6].part[0].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFD5AB, gradientMap: gradient }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(140, 36, 10, 0, Math.PI * 2, 0, Math.PI * 0.21);
  tempGeometry[tempGeometry.length - 1].scale(1, 1, 0.7);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -60, 140, 40, 0, 30, 0, 40, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 40, 145, -30, 0, 20, 0, 50, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], 30, 145, 20, 0, 20, 0, 40, 1);
  dragVertices([tempGeometry[tempGeometry.length - 1]], -30, 145, -20, 0, 20, 0, 50, 1);
  tempGeometry[tempGeometry.length - 1].rotateY(3);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5 + 0.11);
  tempGeometry[tempGeometry.length - 1].translate(100, 102, 0);
  graphics.productModule[6].part[1].filling = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFD5AB, gradientMap: gradient }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(10, 12, 4, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-10, 80, 20);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(12, 12, 4, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 1.5, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-24, 120, -30);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(12, 12, 4, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(-Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-7, 50, 10);
  graphics.productModule[6].part[0].filling_1 = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFB369, gradientMap: gradient }));
  clearTempGeometries();
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(10, 12, 4, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].translate(-34, 70, -30);
  tempGeometry[tempGeometry.length] = new THREE.SphereGeometry(14, 12, 4, 0, Math.PI * 2, 0, Math.PI * 0.5);
  tempGeometry[tempGeometry.length - 1].scale(1, 0.7, 1);
  tempGeometry[tempGeometry.length - 1].rotateZ(Math.PI * 0.5 - 0.3);
  tempGeometry[tempGeometry.length - 1].translate(-35, 140, 20);
  graphics.productModule[6].part[1].filling_1 = new THREE.Mesh(new BufferGeometryUtils.mergeGeometries(tempGeometry), new THREE.MeshToonMaterial({ color: 0xFFB369, gradientMap: gradient }));
  clearTempGeometries();
  graphics.productModule[6].part[0].add(graphics.productModule[6].part[0].outerBody, graphics.productModule[6].part[0].top, graphics.productModule[6].part[0].top_1, graphics.productModule[6].part[0].filling, graphics.productModule[6].part[0].filling_1);
  graphics.productModule[6].part[1].add(graphics.productModule[6].part[1].outerBody, graphics.productModule[6].part[1].top, graphics.productModule[6].part[1].top_1, graphics.productModule[6].part[1].filling, graphics.productModule[6].part[1].filling_1);
  graphics.productModule[6].part[0].outerBody.geometry.translate(90, -110, 0);
  graphics.productModule[6].part[0].top.geometry.translate(90, -110, 0);
  graphics.productModule[6].part[0].top_1.geometry.translate(90, -110, 0);
  graphics.productModule[6].part[0].filling.geometry.translate(90, -110, 0);
  graphics.productModule[6].part[0].filling_1.geometry.translate(90, -110, 0);
  graphics.productModule[6].part[1].outerBody.geometry.translate(-70, -110, 0);
  graphics.productModule[6].part[1].top.geometry.translate(-70, -110, 0);
  graphics.productModule[6].part[1].top_1.geometry.translate(-70, -110, 0);
  graphics.productModule[6].part[1].filling.geometry.translate(-70, -110, 0);
  graphics.productModule[6].part[1].filling_1.geometry.translate(-70, -110, 0);
  graphics.productModule[6].part[0].position.x = -90;
  graphics.productModule[6].part[1].position.x = 70;
  graphics.productModule[6].part[0].container = new THREE.Object3D();
	graphics.productModule[6].part[0].container.add(graphics.productModule[6].part[0]);
	graphics.productModule[6].part[1].container = new THREE.Object3D();
  graphics.productModule[6].part[1].container.add(graphics.productModule[6].part[1]);
  function dragVertices(geometries, fromX, fromY, fromZ, offsetX, offsetY, offsetZ, dragRadius, smoothness) {
  	const dragPoint = new THREE.Vector3(fromX, fromY, fromZ);
  	for (let i = 0; i < geometries.length; i++) {
      for (let j = 0; j < geometries[i].attributes.position.count; j++) {
        if (dragPoint.distanceTo(new THREE.Vector3(geometries[i].attributes.position.getX(j), geometries[i].attributes.position.getY(j), geometries[i].attributes.position.getZ(j))) < dragRadius) {
        	const dragStrength = (1 - dragPoint.distanceTo(new THREE.Vector3(geometries[i].attributes.position.getX(j), geometries[i].attributes.position.getY(j), geometries[i].attributes.position.getZ(j))) / dragRadius) ** smoothness;
        	geometries[i].attributes.position.setXYZ(j, geometries[i].attributes.position.getX(j) + offsetX * dragStrength, geometries[i].attributes.position.getY(j) + offsetY * dragStrength, geometries[i].attributes.position.getZ(j) + offsetZ * dragStrength);
        }
      }
  	}
  }
  graphics.productArray = [];
  graphics.productArea[0] = new THREE.Mesh(new THREE.SphereGeometry(70, 8, 8), transparentMaterial);
  graphics.productArea[1] = graphics.productArea[0].clone();
	graphics.productArea[2] = new THREE.Mesh(new THREE.SphereGeometry(70, 8, 8), transparentMaterial);
  graphics.productArea[2].geometry.translate(0, 90, 0);
  graphics.productArea[2].geometry.scale(2, 1.3, 0.8);
  bendGeometry(graphics.productArea[2].geometry, "z", -0.01);
  graphics.productArea[2].geometry.translate(0, -110, 0);
  graphics.productArea[4] = new THREE.Mesh(new THREE.CylinderGeometry(60, 60, 260, 8, 1, true), transparentMaterial);
  graphics.productArea[6] = new THREE.Mesh(new THREE.SphereGeometry(90, 10, 8), transparentMaterial);
  graphics.productArea[6].geometry.rotateZ(-Math.PI * 0.5);
  graphics.productArea[6].geometry.translate(0, 90, 0);
  graphics.productArea[6].geometry.scale(1, 1, 0.7);
  bendGeometry(graphics.productArea[6].geometry, "z", -0.009);
  graphics.productArea[6].geometry.translate(0, -110, 0);
  
  graphics.poisonModule = [];
  graphics.poisonModule[0] = new THREE.Object3D();
	graphics.poisonModule[0].part = [new THREE.Object3D(), new THREE.Object3D()];
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 0, 90, 65, Math.PI * 0.58, Math.PI * 2.42);
	shape.lineTo(22, 120);
	shape.lineTo(32, 119);
	shape.lineTo(22, 155);
	shape.lineTo(-22, 153);
	shape.lineTo(-36, 119);
	shape.lineTo(-23, 120);
	graphics.poisonModule[0].part[0].outerBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 48), new THREE.MeshBasicMaterial({ color: 0x9477E6 }));

  graphics.poisonModule[0].area = new THREE.Mesh(new THREE.SphereGeometry(85, 8, 8), transparentMaterial);
  graphics.poisonModule[0].area.geometry.scale(1, 65 / 90, 1);
	shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 0, 83, 57, Math.PI * 0.56, Math.PI * 2.44);
	shape.lineTo(15, 128);
	shape.lineTo(22, 127);
	shape.lineTo(15, 147);
	shape.lineTo(-17, 145);
	shape.lineTo(-25, 127);
	shape.lineTo(-15, 128);
	graphics.poisonModule[0].part[0].innerBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 48), new THREE.MeshBasicMaterial({ color: 0x572382 }));
  graphics.poisonModule[0].part[0].innerBody.geometry.translate(0, 0, 0.1);
	dragVertices([graphics.poisonModule[0].part[0].innerBody.geometry, graphics.poisonModule[0].part[0].outerBody.geometry], -85, -5, 0, -5, 7, 0, 90, 1);
  dragVertices([graphics.poisonModule[0].part[0].innerBody.geometry, graphics.poisonModule[0].part[0].outerBody.geometry], 85, -5, 0, 5, -7, 0, 90, 1);
  brush1 = new Brush(new THREE.SphereGeometry(75, 48, 64), new THREE.MeshToonMaterial({ color: 0x309BF2, gradientMap: gradient }));
  brush1.geometry.scale(1, 0.67, 1);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.CylinderGeometry(200, 200, 200, 48, 1), new THREE.MeshToonMaterial({ color: 0x0982E5, gradientMap: gradient }));
  brush2.geometry.rotateX(Math.PI * 0.5);
  brush2.geometry.translate(0, 205, 0);
  brush2.geometry.scale(1, 1, 1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[0].part[0].water = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  graphics.poisonModule[0].part[0].water.position.set(0, 3, 75);
  gsap.to(graphics.poisonModule[0].part[0].water.rotation, { duration: 5, y: Math.PI * 2, ease: "none", repeat: -1, yoyo: true });
  shape = null;
  shape = new THREE.Shape();
  shape.absellipse(0, 0, 55, 50, Math.PI * 0.6, Math.PI * 1.1);
  shape.absellipse(0, 0, 30, 50, Math.PI * 1.1, Math.PI * 0.6, true);
  graphics.poisonModule[0].part[0].highlight = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0.3 }));
	graphics.poisonModule[0].part[0].highlight.position.set(0, 0, 150);
	shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-40, 55);
  shape.lineTo(-17, 50);
	shape.lineTo(-5, 85);
	shape.lineTo(8, 50);
  shape.lineTo(11, 60);
  shape.lineTo(14, 50);
  shape.lineTo(18, 55);
  shape.lineTo(30, 45);
  shape.lineTo(60, 47);
  shape.lineTo(600, 47);
  shape.lineTo(1000, -1000);
  shape.lineTo(-1000, -1000);
  shape.lineTo(-500, 70);
  shape.lineTo(-50, 65);
	brush1 = new Brush(graphics.poisonModule[0].part[0].outerBody.geometry, new THREE.MeshBasicMaterial({ color: 0x9477E6 }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[0].part[1].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.poisonModule[0].part[0].innerBody.geometry, new THREE.MeshBasicMaterial({ color: 0x572382 }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[0].part[1].innerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
	shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-40, 55);
  shape.lineTo(-17, 50);
	shape.lineTo(-5, 85);
	shape.lineTo(8, 50);
  shape.lineTo(11, 60);
  shape.lineTo(14, 50);
  shape.lineTo(18, 55);
  shape.lineTo(30, 45);
  shape.lineTo(60, 47);
  shape.lineTo(1000, 1000);
  shape.lineTo(-1000, 1000);
	brush1 = new Brush(graphics.poisonModule[0].part[0].outerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[0].part[0].outerBody.geometry.dispose();
  graphics.poisonModule[0].part[0].outerBody.geometry = evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry;
  brush1 = new Brush(graphics.poisonModule[0].part[0].innerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[0].part[0].innerBody.geometry.dispose();
  graphics.poisonModule[0].part[0].innerBody.geometry = evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry;
	graphics.poisonModule[0].part[1].add(graphics.poisonModule[0].part[1].outerBody, graphics.poisonModule[0].part[1].innerBody);
	graphics.poisonModule[0].part[0].add(graphics.poisonModule[0].part[0].outerBody, graphics.poisonModule[0].part[0].highlight, graphics.poisonModule[0].part[0].innerBody, graphics.poisonModule[0].part[0].water);
  graphics.poisonModule[1] = new THREE.Object3D();
	graphics.poisonModule[1].part = [new THREE.Object3D(), new THREE.Object3D()];
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(90, -65);
  shape.lineTo(22, 65);
	shape.lineTo(21, 155);
	shape.lineTo(-22, 153);
	shape.lineTo(-22, 65);
	shape.lineTo(-90, -65);
	graphics.poisonModule[1].part[0].outerBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 48), new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
	shape = null;
  shape = new THREE.Shape();
  shape.moveTo(76, -58);
	shape.lineTo(14, 65);
	shape.lineTo(14, 147);
	shape.lineTo(-14, 146);
	shape.lineTo(-14, 65);
	shape.lineTo(-76, -58);
	graphics.poisonModule[1].part[0].innerBody = new THREE.Mesh(new THREE.ShapeGeometry(shape, 48), new THREE.MeshBasicMaterial({ color: 0x80C1FF }));
  graphics.poisonModule[1].part[0].innerBody.geometry.translate(0, 0, 0.1);
  brush1 = new Brush(new THREE.CylinderGeometry(14, 76, 123, 56, 1), new THREE.MeshBasicMaterial({ color: 0xC7B3FF, side: THREE.BackSide }));
  brush1.geometry.translate(0, 0.7, 0);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.CylinderGeometry(100, 100, 200, 48, 1), new THREE.MeshBasicMaterial({ color: 0x309BF2 }));
  brush2.geometry.rotateX(Math.PI * 0.5);
  brush2.geometry.translate(20, 100, 0);
  brush2.geometry.scale(1, 1, 1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[1].part[0].water = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  graphics.poisonModule[1].part[0].water.position.set(0, 3, 77);
  gsap.to(graphics.poisonModule[1].part[0].water.rotation, { duration: 6, y: -Math.PI * 2, ease: "none", repeat: -1, yoyo: true });
 
  graphics.poisonModule[1].area = new THREE.Mesh(new THREE.CylinderGeometry(14, 76, 123, 12, 1), transparentMaterial);

  shape = null;
  shape = new THREE.Shape();
  shape.lineTo(-10, -75);
  shape.lineTo(-40, -70);
  

  graphics.poisonModule[1].part[0].highlight = new THREE.Mesh(new THREE.ShapeGeometry(shape, 16), new THREE.MeshBasicMaterial({ color: 0xFFFFFF, transparent: true, opacity: 0.4 }));
	graphics.poisonModule[1].part[0].highlight.position.set(-9, 40, 150);
	shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-40, 55);
  shape.lineTo(-17, 50);
	shape.lineTo(-5, 85);
	shape.lineTo(8, 50);
  shape.lineTo(11, 60);
  shape.lineTo(14, 50);
  shape.lineTo(18, 55);
  shape.lineTo(30, 45);
  shape.lineTo(60, 47);
  shape.lineTo(600, 47);
  shape.lineTo(1000, -1000);
  shape.lineTo(-1000, -1000);
  shape.lineTo(-500, 70);
  shape.lineTo(-50, 65);
	brush1 = new Brush(graphics.poisonModule[1].part[0].outerBody.geometry, new THREE.MeshBasicMaterial({ color: 0xFFFFFF }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[1].part[1].outerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
  brush1 = new Brush(graphics.poisonModule[1].part[0].innerBody.geometry, new THREE.MeshBasicMaterial({ color: 0x80C1FF }));
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[1].part[1].innerBody = evaluator.evaluate(brush1, brush2, SUBTRACTION);
	shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-40, 55);
  shape.lineTo(-17, 50);
	shape.lineTo(-5, 85);
	shape.lineTo(8, 50);
  shape.lineTo(11, 60);
  shape.lineTo(14, 50);
  shape.lineTo(18, 55);
  shape.lineTo(30, 45);
  shape.lineTo(60, 47);
  shape.lineTo(1000, 1000);
  shape.lineTo(-1000, 1000);
	brush1 = new Brush(graphics.poisonModule[1].part[0].outerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[1].part[0].outerBody.geometry.dispose();
  graphics.poisonModule[1].part[0].outerBody.geometry = evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry;
  brush1 = new Brush(graphics.poisonModule[1].part[0].innerBody.geometry);
  brush1.updateMatrixWorld();
  brush2 = new Brush(new THREE.ExtrudeGeometry(shape, {steps: 1, depth: 2, curveSegments: 1, bevelEnabled: false }));
  brush2.geometry.translate(0, 0, -1);
  brush2.updateMatrixWorld();
  graphics.poisonModule[1].part[0].innerBody.geometry.dispose();
  graphics.poisonModule[1].part[0].innerBody.geometry = evaluator.evaluate(brush1, brush2, SUBTRACTION).geometry;
  graphics.poisonModule[1].part[0].bubble_1 = new THREE.Mesh(new THREE.CircleGeometry(12, 24), new THREE.MeshBasicMaterial({ color: 0x9577E6 }));
  graphics.poisonModule[1].part[0].bubble_1.position.set(-35, -30, 75);
  graphics.poisonModule[1].part[0].bubble_2 = new THREE.Mesh(new THREE.CircleGeometry(10, 24), new THREE.MeshBasicMaterial({ color: 0x9577E6 }));
  graphics.poisonModule[1].part[0].bubble_2.position.set(40, -40, 75);
  graphics.poisonModule[1].part[0].bubble_3 = new THREE.Mesh(new THREE.CircleGeometry(8, 16), new THREE.MeshBasicMaterial({ color: 0x9577E6 }));
  graphics.poisonModule[1].part[0].bubble_3.position.set(5, -20, 75);
  gsap.to(graphics.poisonModule[1].part[0].bubble_1.position, { duration: 0.7 + Math.random() * 0.7, x: graphics.poisonModule[1].part[0].bubble_1.position.x + 5, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(graphics.poisonModule[1].part[0].bubble_1.position, { duration: 0.7 + Math.random() * 0.7, y: graphics.poisonModule[1].part[0].bubble_1.position.y + 5, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(graphics.poisonModule[1].part[0].bubble_2.position, { duration: 0.7 + Math.random() * 0.7, x: graphics.poisonModule[1].part[0].bubble_2.position.x - 5, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(graphics.poisonModule[1].part[0].bubble_2.position, { duration: 0.7 + Math.random() * 0.7, y: graphics.poisonModule[1].part[0].bubble_2.position.y + 5, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(graphics.poisonModule[1].part[0].bubble_3.position, { duration: 0.7 + Math.random() * 0.7, x: graphics.poisonModule[1].part[0].bubble_3.position.x - 5, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(graphics.poisonModule[1].part[0].bubble_3.position, { duration: 0.7 + Math.random() * 0.7, y: graphics.poisonModule[1].part[0].bubble_3.position.y + 5, ease: "power1.inOut", repeat: -1, yoyo: true });
	graphics.poisonModule[1].part[1].add(graphics.poisonModule[1].part[1].outerBody, graphics.poisonModule[1].part[1].innerBody);
	graphics.poisonModule[1].part[0].add(graphics.poisonModule[1].part[0].bubble_2, graphics.poisonModule[1].part[0].bubble_3, graphics.poisonModule[1].part[0].outerBody, graphics.poisonModule[1].part[0].bubble_1, graphics.poisonModule[1].part[0].highlight, graphics.poisonModule[1].part[0].innerBody, graphics.poisonModule[1].part[0].water);
	
	graphics.poisonModule[0].part[0].scale.set(0.6, 0.6, 1);
  graphics.poisonModule[0].part[1].scale.set(0.6, 0.6, 1);
  graphics.poisonModule[0].area.scale.set(0.6, 0.6, 1);

	graphics.poisonModule[1].part[0].scale.set(0.6, 0.6, 1);
  graphics.poisonModule[1].part[1].scale.set(0.6, 0.6, 1);
  graphics.poisonModule[1].area.scale.set(0.6, 0.6, 1);

  graphics.poisonModule[0].add(graphics.poisonModule[0].part[0], graphics.poisonModule[0].part[1], graphics.poisonModule[0].area);
  graphics.poisonModule[1].add(graphics.poisonModule[1].part[0], graphics.poisonModule[1].part[1], graphics.poisonModule[1].area);

	graphics.poisonModule[0].FX_1 = new THREE.Mesh(new THREE.PlaneGeometry(tex[26].image.width * 0.3, tex[26].image.height * 0.3), new THREE.MeshBasicMaterial({ map: tex[26], transparent: true, opacity: 0 }))
  graphics.poisonModule[0].FX_2 = new THREE.Mesh(new THREE.PlaneGeometry(tex[27].image.width * 0.3, tex[27].image.height * 0.3), new THREE.MeshBasicMaterial({ map: tex[27], transparent: true, opacity: 0 }))
  graphics.poisonModule[1].FX_1 = new THREE.Mesh(new THREE.PlaneGeometry(tex[28].image.width * 0.3, tex[28].image.height * 0.3), new THREE.MeshBasicMaterial({ map: tex[28], transparent: true, opacity: 0 }))
  graphics.poisonModule[1].FX_2 = new THREE.Mesh(new THREE.PlaneGeometry(tex[29].image.width * 0.3, tex[29].image.height * 0.3), new THREE.MeshBasicMaterial({ map: tex[29], transparent: true, opacity: 0 }))

	checkLoading();
	
	
	/*setTimeout(function() {
		
		dropProduct();
		onGame = true;
	}, 100);*/
	
	
	
}


function hideLoadingScreen() {
  mainScene.remove(graphics.loadingBar);
	gsap.fromTo(graphics.logo.children[4].position, { y: 0 }, { duration: 0.3, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[1].position, { y: 0 }, { duration: 0.35, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[3].position, { y: 0 }, { duration: 0.4, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[5].position, { y: 0 }, { duration: 0.45, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[0].position, { y: 0 }, { duration: 0.5, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[6].position, { y: 0 }, { duration: 0.55, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[2].position, { y: 0 }, { duration: 0.6, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.fromTo(graphics.logo.children[7].position, { y: 0 }, { duration: 0.65, y: -6, ease: "power1.inOut", repeat: 1, yoyo: true });
  gsap.to(graphics.logo.position, { duration: 0.5, x: -60, y: -35, ease: "power1.inOut" });
  gsap.to(graphics.logo.scale, { duration: 0.5, x: 0.6667, y: -0.6667, ease: "power1.inOut", onComplete() {
  	showStartScreen();
  } });
  for (let i = 0; i < 6; i++) {
  	graphics.backApple[i].rotTween = gsap.to(graphics.backApple[i].rotation, { duration: 1 + Math.random(), z: graphics.backApple[i].rotation.z - 0.2 + Math.round(Math.random() * 0.4), ease: "power1.inOut", repeat: -1, yoyo: true });
   	gsap.to(graphics.backApple[i].position, { duration: 0.3 + Math.random() * 0.5, x: graphics.backApple[i].position.x * 0.8, y: graphics.backApple[i].position.y * 0.8, ease: "power1.inOut", repeat: 1, yoyo: true, onComplete() {
   		graphics.backApple[i].tweenX = gsap.to(graphics.backApple[i].position, { duration: 1 + Math.random(), x: graphics.backApple[i].position.x + 15, ease: "power1.inOut", repeat: -1, yoyo: true });
      graphics.backApple[i].tweenY = gsap.to(graphics.backApple[i].position, { duration: 1 + Math.random(), y: graphics.backApple[i].position.y + 15, ease: "power1.inOut", repeat: -1, yoyo: true });
   	} });
  }
}
function showStartScreen() {
  for (let i = 0; i < graphics.title.line_1.symbol.length; i++) {
  	gsap.from(graphics.title.line_1.symbol[i].position, { duration: 0.2 + 0.03 * i, x: graphics.title.line_1.symbol[i].position.x + 100, ease: "power1.in" });
    gsap.to(graphics.title.line_1.symbol[i].material, { duration: 0.2 + 0.03 * i, opacity: 1, ease: "power1.in" });
  }
  gsap.to(graphics.title.line_1.position, { duration: 0.3, x: graphics.title.line_1.position.x - 30, ease: "power1.out", repeat: 1, yoyo: true, delay: 0.2 });
  for (let i = 0; i < graphics.title.line_2.symbol.length; i++) {
    gsap.from(graphics.title.line_2.symbol[i].position, { duration: 0.2 + 0.03 * (graphics.title.line_2.symbol.length - i), x: graphics.title.line_2.symbol[i].position.x - 100, ease: "power1.in", delay: 0.5 });
    gsap.to(graphics.title.line_2.symbol[i].material, { duration: 0.2 + 0.03 * (graphics.title.line_2.symbol.length - i), opacity: 1, ease: "power1.in", delay: 0.5 });
  }
  gsap.to(graphics.title.line_2.position, { duration: 0.3, x: graphics.title.line_1.position.x + 30, ease: "power1.out", repeat: 1, yoyo: true, delay: 0.7 });
  for (let i = 0; i < text[1].length; i++) {
    gsap.from(graphics.startText.line[i].position, { duration: 0.25 + 0.05 * i, y: graphics.startText.line[i].position.y - 20, ease: "power1.in", delay: 1 });
    gsap.to(graphics.startText.line[i].material, { duration: 0.25 + 0.05 * i, opacity: 1, ease: "power1.in", delay: 1 });
  }
  gsap.to(graphics.startBubble.scale, { duration: 0.4, x: 1, y: 1, ease: "back.out", delay: 1.5 });
  graphics.startBubble.bubble.tween = gsap.fromTo(graphics.startBubble.bubble.rotation, { z: -0.02 }, { duration: 1, z: 0.02, ease: "power1.inOut", repeat: -1, yoyo: true });
  mainScene.add(graphics.title, graphics.startText, graphics.startBubble);
	bottomContainer.add(graphics.button[0]);
  gsap.to(graphics.button[0].position, { duration: 0.3, y: 70, ease: "power1.out", delay: 2, onComplete() { 
  	graphics.button[0].ready = true;
  	graphics.title.line_1.visible = false;
  	graphics.title.line_2.visible = false;
  	graphics.title.part_1.visible = true;
  	graphics.title.part_2.visible = true;
  } });
  document.body.addEventListener('touchstart', onDocumentTouchStart, false);
  document.body.addEventListener('touchmove', onDocumentTouchMove, false);
  document.body.addEventListener('touchend', onDocumentTouchEnd, false);
}
function hideStartScreen() {
	graphics.button[0].ready = false;
	gsap.to(graphics.button[0].clickableArea.material.color, { duration: 0.15, r: darkRedColor.r, g: darkRedColor.g, b: darkRedColor.b, ease: "power1.out", repeat: 1, yoyo: true });
  gsap.to(graphics.button[0].back.material.color, { duration: 0.15, r: redShadowColor.r, g: redShadowColor.g, b: redShadowColor.b, ease: "power1.out", repeat: 1, yoyo: true });
	gsap.to(graphics.button[0].clickableArea.position, { duration: 0.15, y: graphics.button[0].clickableArea.position.y - 6, ease: "power2.out", repeat: 1, yoyo: true });
  gsap.to(graphics.button[0].text.position, { duration: 0.15, y: graphics.button[0].text.position.y - 6, ease: "power2.out", repeat: 1, yoyo: true });
  gsap.to(graphics.button[0].back.position, { duration: 0.15, y: graphics.button[0].back.position.y + 6, ease: "power2.out", repeat: 1, yoyo: true, onComplete() {
  	gsap.to(graphics.button[0].position, { duration: 0.3, y: -35, ease: "power2.in", onComplete() {
  		bottomContainer.remove(graphics.button[0]);
  		goTutorial();
  	} });
  } });
  for (let i = 0; i < graphics.backApple.length; i++) {
  	graphics.backApple[i].rotTween.kill();
  	graphics.backApple[i].rotTween = null;
  	graphics.backApple[i].tweenX.kill();
  	graphics.backApple[i].tweenX = null;
  	graphics.backApple[i].tweenY.kill();
  	graphics.backApple[i].tweenY = null;
    gsap.to(graphics.backApple[i].position, { duration: 0.6 + Math.random() * 0.6, x: graphics.backApple[i].position.x * 4, y: graphics.backApple[i].position.y * 4, ease: "power2.in", onComplete() {
    	mainScene.remove(graphics.backApple[i]);
    } });
  }
  gsap.to(graphics.startBubble.scale, { duration: 0.3, x: 0, y: 0, ease: "back.in", onComplete() {
  	graphics.startBubble.bubble.tween.kill();
  	graphics.startBubble.bubble.tween = null;
  	mainScene.remove(graphics.startBubble);
  } });
  for (let i = 0; i < text[1].length; i++) {
  	gsap.to(graphics.startText.line[i].position, { duration: 0.25 + 0.05 * (text[1].length - i), y: graphics.startText.line[i].position.y - 20, ease: "power1.in" });
  	gsap.to(graphics.startText.line[i].material, { duration: 0.25 + 0.05 * (text[1].length - i), opacity: 0, ease: "power1.inOut" });
  }
  goCutFX(0, 120, 1);
  gsap.to(graphics.title.part_1.rotation, { duration: 1.5, z: 3, ease: "power1.out" });
  gsap.to(graphics.title.part_2.rotation, { duration: 1.5, z: -3, ease: "power1.out" });
  gsap.to([graphics.title.part_1.material, graphics.title.part_2.material], { duration: 1.5, opacity: 0, ease: "power1.out" });
  gsap.to(graphics.title.part_1.position, { duration: 1.5, x: graphics.title.part_1.position.x * 4, y: graphics.title.part_1.position.y + 100, ease: "power1.out" });
  gsap.to(graphics.title.part_2.position, { duration: 1.5, x: graphics.title.part_2.position.x * 4, y: graphics.title.part_2.position.y - 100, ease: "power1.out", onComplete: function() {
  	mainScene.remove(graphics.title);
  } });
}
function goTutorial() {
	for (let i = 0; i < 7; i++) {
		gsap.to(graphics.backLine[i].position, { duration: 1.2 + 0.3 * i, x: 2940 - 250 * i, ease: "back.out" });
	}
/*	*/
  
  
  graphics.productArray[graphics.productArray.length] = [];
  graphics.productArray[graphics.productArray.length - 1][8] = 0;
  graphics.productArray[graphics.productArray.length - 1][0] = graphics.productModule[graphics.productArray[graphics.productArray.length - 1][8]].part[0].container.clone();
  graphics.productArray[graphics.productArray.length - 1][1] = graphics.productModule[graphics.productArray[graphics.productArray.length - 1][8]].part[1].container.clone();
  graphics.productArray[graphics.productArray.length - 1][2] = graphics.productArea[graphics.productArray[graphics.productArray.length - 1][8]].clone();

  for (let i = 0; i < productZOrder.length; i++) {
    if (productZOrder[i]) {
    	productZOrder[i] = false;
    	graphics.productArray[graphics.productArray.length - 1][5] = i;
    	break;
  	}
  }

  graphics.productArray[graphics.productArray.length - 1][0].position.set(-400, -400, -5000 - 200 * graphics.productArray[graphics.productArray.length - 1][5]);
  graphics.productArray[graphics.productArray.length - 1][1].position.set(-400, -400, -5000 - 200 * graphics.productArray[graphics.productArray.length - 1][5]);
  graphics.productArray[graphics.productArray.length - 1][2].position.set(-400, -400, -5000 - 200 * graphics.productArray[graphics.productArray.length - 1][5]);

  graphics.productArray[graphics.productArray.length - 1][0].children[0].rotation.z = Math.random() * 6;
  graphics.productArray[graphics.productArray.length - 1][1].children[0].rotation.z = graphics.productArray[graphics.productArray.length - 1][0].children[0].rotation.z;
  graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.55, 0.55, 0.55);
  graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.55, 0.55, 0.55);
  graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.55, 0.55, 0.55);


  graphics.productArray[graphics.productArray.length - 1][3] = [200, 720];
  graphics.productArray[graphics.productArray.length - 1][7] = [graphics.productArray[graphics.productArray.length - 1][3][0], graphics.productArray[graphics.productArray.length - 1][3][1]];

   
  graphics.productArray[graphics.productArray.length - 1][4] = "solid";
  graphics.productArray[graphics.productArray.length - 1].tweenY = gsap.to([graphics.productArray[graphics.productArray.length - 1][0].rotation, graphics.productArray[graphics.productArray.length - 1][1].rotation, graphics.productArray[graphics.productArray.length - 1][2].rotation], { duration: 1 + Math.random() * 1, y: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
  graphics.productArray[graphics.productArray.length - 1].tweenZ = gsap.to([graphics.productArray[graphics.productArray.length - 1][0].rotation, graphics.productArray[graphics.productArray.length - 1][1].rotation, graphics.productArray[graphics.productArray.length - 1][2].rotation], { duration: 1 + Math.random() * 1, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
  graphics.productArray[graphics.productArray.length - 1][6] = [0, Math.PI * 0.5];
  mainScene.add(graphics.productArray[graphics.productArray.length - 1][0], graphics.productArray[graphics.productArray.length - 1][1], graphics.productArray[graphics.productArray.length - 1][2]);

  
  
  onFly = true;
  
  
  

  
  
	
}

function showHint() {
	
	for (let i = 0; i < text[2].length; i++) {
		gsap.from(graphics.hintText_1.line[i].position, { duration: 0.25 + 0.05 * i, y: graphics.hintText_1.line[i].position.y - 20, ease: "power1.in" });
		gsap.to(graphics.hintText_1.line[i].material, { duration: 0.25 + 0.05 * i, opacity: 1, ease: "power1.in" });
	}
	mainScene.add(graphics.hintText_1);
	
	onTrail = true;
  const hintMove = { posX: -120, posY: -60 };
  const tempTranslationMatrix = new THREE.Matrix4();
  tempTranslationMatrix.makeTranslation(hintMove.posX, hintMove.posY, 0);
  const tempRotationMatrix = new THREE.Matrix4();
  trailTarget.matrix.identity();
  trailTarget.applyMatrix4(tempTranslationMatrix);
  trailTarget.updateMatrixWorld();
  trailTarget.oldPos = [hintMove.posX, hintMove.posY];
  trail.initialize(trailMaterial, Math.round(50 * fps / 120), 0, 0, headGeometry, trailTarget);
  trail.activate();

  graphics.hintTween = gsap.to(hintMove, { duration: 1.2, posX: 120, posY: 140, ease: "power1.inOut", repeatDelay: 1, repeat: -1, onRepeat() {
  	const tempTranslationMatrix = new THREE.Matrix4();
    tempTranslationMatrix.makeTranslation(hintMove.posX, hintMove.posY, 0);
    const tempRotationMatrix = new THREE.Matrix4();
    trailTarget.matrix.identity();
    trailTarget.applyMatrix4(tempTranslationMatrix);
    trailTarget.updateMatrixWorld();
    trailTarget.oldPos = [hintMove.posX, hintMove.posY];
    trail.initialize(trailMaterial, Math.round(50 * fps / 120), 0, 0, headGeometry, trailTarget);
    trail.activate();
  	
  }, onUpdate() {
  	const tempTranslationMatrix = new THREE.Matrix4();
  	tempTranslationMatrix.makeTranslation(hintMove.posX, hintMove.posY, 0);
  	const tempRotationMatrix = new THREE.Matrix4();
  	moveAngle = new THREE.Vector2(240, 200).angle() - Math.PI * 0.5;
  	tempRotationMatrix.makeRotationZ(moveAngle);
  	tempTranslationMatrix.multiply(tempRotationMatrix);
  	trailMaterial.uniforms.headColor.value.w = new THREE.Vector2(hintMove.posX, hintMove.posY).distanceTo(new THREE.Vector2(trailTarget.oldPos[0], trailTarget.oldPos[1])) / 0.15 * (window.innerWidth * window.devicePixelRatio / 1602);
  	if (trailMaterial.uniforms.headColor.value.w > 0.5) {
  		trailMaterial.uniforms.headColor.value.w = 0.5;
  	}
  	trailTarget.oldPos = [hintMove.posX, hintMove.posX];
  	trailTarget.matrix.identity();
  	trailTarget.applyMatrix4(tempTranslationMatrix);
  	trailTarget.updateMatrixWorld();
  } });
}


function showHint_2() {
	for (let i = 0; i < text[2].length; i++) {
		gsap.to(graphics.hintText_1.line[i].position, { duration: 0.25 + 0.05 * (text[2].length - i), y: graphics.hintText_1.line[i].position.y - 10, ease: "power1.in" });
		gsap.to(graphics.hintText_1.line[i].material, { duration: 0.25 + 0.05 * (text[2].length - i), opacity: 0, ease: "power1.in" });
	}
	
	
	
  
  
  graphics.poisonModule[0].position.set(-400, -400, -400);
  graphics.poisonModule[0].speed = [200, 650];
  graphics.poisonModule[0].stat = "solid";
  graphics.poisonModule[0].tweenZ = gsap.fromTo(graphics.poisonModule[0].rotation, { z: -0.5 }, { duration: 1 + Math.random() * 1, z: 0.5, ease: "power1.inOut", repeat: -1, yoyo: true });
	graphics.poisonModule[1].position.set(400, -400, -600);
  graphics.poisonModule[1].speed = [-200, 730];
  graphics.poisonModule[1].stat = "solid";
  graphics.poisonModule[1].tweenZ = gsap.fromTo(graphics.poisonModule[1].rotation, { z: -0.5 }, { duration: 1 + Math.random() * 1, z: 0.5, ease: "power1.inOut", repeat: -1, yoyo: true });

	
	mainScene.add(graphics.poisonModule[0], graphics.poisonModule[1]);
	
	

}
function showHint_3() {
	mainScene.remove(graphics.hintText_1);
	for (let i = 0; i < text[3].length; i++) {
		gsap.from(graphics.hintText_2.line[i].position, { duration: 0.25 + 0.05 * i, y: graphics.hintText_2.line[i].position.y - 20, ease: "power1.in" });
		gsap.to(graphics.hintText_2.line[i].material, { duration: 0.25 + 0.05 * i, opacity: 1, ease: "power1.in" });
	}
	mainScene.add(graphics.hintText_2);
  bottomContainer.add(graphics.button[1]);
  gsap.to(graphics.button[1].position, { duration: 0.3, y: 70, ease: "power1.out", onComplete() { 
  	graphics.button[1].ready = true;
  	
  } });
}

function hideTutorial() {
	graphics.button[1].ready = false;
	onFly = true;
	gsap.to(graphics.button[1].clickableArea.material.color, { duration: 0.15, r: darkRedColor.r, g: darkRedColor.g, b: darkRedColor.b, ease: "power1.out", repeat: 1, yoyo: true });
  gsap.to(graphics.button[1].back.material.color, { duration: 0.15, r: redShadowColor.r, g: redShadowColor.g, b: redShadowColor.b, ease: "power1.out", repeat: 1, yoyo: true });
	gsap.to(graphics.button[1].clickableArea.position, { duration: 0.15, y: graphics.button[1].clickableArea.position.y - 6, ease: "power2.out", repeat: 1, yoyo: true });
  gsap.to(graphics.button[1].text.position, { duration: 0.15, y: graphics.button[1].text.position.y - 6, ease: "power2.out", repeat: 1, yoyo: true });
  gsap.to(graphics.button[1].back.position, { duration: 0.15, y: graphics.button[1].back.position.y + 6, ease: "power2.out", repeat: 1, yoyo: true, onComplete() {
  	gsap.to(graphics.button[1].position, { duration: 0.3, y: -35, ease: "power2.in", onComplete() {
  		bottomContainer.remove(graphics.button[1]);
  		goGame();
  	} });
  } });
  for (let i = 0; i < text[3].length; i++) {
		gsap.to(graphics.hintText_2.line[i].position, { duration: 0.25 + 0.05 * (text[3].length - i), y: graphics.hintText_2.line[i].position.y - 10, ease: "power1.in" });
		gsap.to(graphics.hintText_2.line[i].material, { duration: 0.25 + 0.05 * (text[3].length - i), opacity: 0, ease: "power1.in" });
	}
	if (graphics.poisonModule[0].tweenZ !== undefined && graphics.poisonModule[0].tweenZ !== null) {
	  graphics.poisonModule[0].tweenZ.play();
	}
  if (graphics.poisonModule[1].tweenZ !== undefined && graphics.poisonModule[1].tweenZ !== null) {
  	graphics.poisonModule[1].tweenZ.play();
  }

}

function goGame() {
	bottomContainer.add(graphics.table, graphics.bowl);
	gsap.to(graphics.table.position, { duration: 0.3, y: -1, ease: "power1.in" });
  gsap.to(graphics.bowl.position, { duration: 0.3, y: 57, ease: "power1.in", onComplete() {
  	gsap.to(graphics.bowl.rotation, { duration: 0.3, z: 0.3, ease: "power1.out", repeat: 1, yoyo: true });
    gsap.to(graphics.bowl.rotation, { duration: 0.6, y: graphics.bowl.rotation.y + Math.PI * 2, ease: "none" });
    dropProduct();
    
    setTimeout(function() {
    	dropPoison();
    }, 3000);
    onGame = true;
    onTrail = true;
  	gsap.to(graphics.bowl.position, { duration: 0.3, y: 200, ease: "power1.out", repeat: 1, yoyo: true, onComplete() {
  	  gsap.to(graphics.bowl.rotation, { duration: 0.1, z: 0.1, ease: "power1.out", repeat: 1, yoyo: true });
      gsap.to(graphics.bowl.rotation, { duration: 0.2, y: graphics.bowl.rotation.y + Math.PI * 0.2, ease: "none" });

  	  gsap.to(graphics.bowl.position, { duration: 0.1, y: 80, ease: "power1.out", repeat: 1, yoyo: true, onComplete() {
  	
      } });
    } });
  } });
}

function goCutFX(posX, posY, angle, isBlot) {
  console.log("cut")
	for (let i = 0; i < 10; i++) {
    if (!graphics.cutFX[i].on) {
    	graphics.cutFX[i].on = true;
    	graphics.cutFX[i].rotation.z = angle;
    	graphics.cutFX[i].pic.scale.set(0, 0, 1);
    	graphics.cutFX[i].back.scale.set(0, 0, 1);
    	const randomMirror = -1 + Math.round(Math.random()) * 2;
    	graphics.cutFX[i].pic.position.x = -tex[14].image.width * 0.15;
    	graphics.cutFX[i].back.position.set(-tex[14].image.width * 0.15, -5 * Math.cos(graphics.cutFX[i].rotation.z), -0.1);
    	gsap.to([graphics.cutFX[i].pic.position, graphics.cutFX[i].back.position], { duration: 0.4, x: tex[14].image.width * 0.15, ease: "power1.inOut" });
      gsap.to([graphics.cutFX[i].pic.scale, graphics.cutFX[i].back.scale], { duration: 0.2, x: 1, y: randomMirror, ease: "power1.in", repeat: 1, yoyo: true, onComplete() {
      	graphics.cutFX[i].on = false;
      	mainScene.remove(graphics.cutFX[i]);
      } });
    	graphics.cutFX[i].position.set(posX, posY, -5 + Math.random());
    	
    	for (let j = 0; j < 10; j++) {
    		if (!graphics.scar[j].on) {
    	    graphics.scar[j].on = true;
    	    
          graphics.scar[j].scale.y = randomMirror;
    	    graphics.scar[j].rotation.z = angle;
    	    graphics.scar[j].position.set(posX, posY, -14900 - Math.random());
    	    gsap.to(graphics.scar[j].material, { duration: 0.15, opacity: 1, ease: "power2.out", onComplete() {
            gsap.to(graphics.scar[j].material, { duration: 1, opacity: 0, ease: "none", onComplete() {
              mainScene.remove(graphics.scar[j]);
              graphics.scar[j].on = false;
            } });
    	    } });
    	    mainScene.add(graphics.scar[j]);
    	    break;
    		}
    	}
    	if (isBlot) {
    	  for (let j = 0; j < 12; j++) {
    		  if (!graphics.blot[j].on) {
    	      graphics.blot[j].on = true;
            graphics.blot[j].scale.set(0, 0, 1);
    	      graphics.blot[j].rotation.z = Math.random() * Math.PI * 2;
    	      graphics.blot[j].position.set(posX - 50 + Math.random() * 100, posY - 50 + Math.random() * 100, -14902 - Math.random());
    	      gsap.to(graphics.blot[j].scale, { duration: 0.15, x: 1, y: 1, ease: "power2.out" });
    	      gsap.to(graphics.blot[j].material, { duration: 0.15, opacity: 1, ease: "power2.out", onComplete() {
              gsap.to(graphics.blot[j].material, { duration: 0.8, opacity: 0, ease: "none", onComplete() {
                mainScene.remove(graphics.blot[j]);
                graphics.blot[j].on = false;
              } });
    	      } });
    	      mainScene.add(graphics.blot[j]);
    	      break;
    		  }
    	  }
    	}
    	
    	
    	
    	mainScene.add(graphics.cutFX[i]);
    	
    	
    	
    	
    	break;
    }
	}
	for (let i = 0; i < 10; i++) {
    if (!graphics.cutSparkle[i].on) {
    	graphics.cutSparkle[i].on = true;
    	for (let j = 0; j < 13; j++) {
    		graphics.cutSparkle[i].pic[j].position.set(0, 0, 0);
    		graphics.cutSparkle[i].pic[j].material.opacity = 0;
    		const randomAngle = Math.random() * Math.PI * 2;
    		graphics.cutSparkle[i].pic[j].rotation.z = randomAngle;
    		const randomTime = 0.2 + Math.random() * 0.4;
    		const randomDist = 50 + Math.random() * 150;
    		gsap.to(graphics.cutSparkle[i].pic[j].position, { duration: randomTime, x: Math.cos(randomAngle) * randomDist, y: Math.sin(randomAngle) * randomDist, ease: "power1.out" });
        gsap.to(graphics.cutSparkle[i].pic[j].material, { duration: randomTime * 0.5, opacity: 1, ease: "power1.in", repeat: 1, yoyo: true });

    		
    	}
    	graphics.cutSparkle[i].position.set(posX, posY, -5 - Math.random());
      gsap.to(graphics.cutSparkle[i].scale, { duration: 0.6, x: 1, ease: "none", onComplete() { graphics.cutSparkle[i].on = false } });

    	mainScene.add(graphics.cutSparkle[i]);
    	break;
    }
    
    
	}
    


	
	
  
  

	
}


function dropPoison() {
	const chosenPoison = Math.round(Math.random());
	const side = -1 + Math.round(Math.random()) * 2;
    
  if (graphics.poisonModule[chosenPoison].tweenZ !== undefined && graphics.poisonModule[chosenPoison].tweenZ !== null) {
  	graphics.poisonModule[chosenPoison].tweenZ.kill();
  	graphics.poisonModule[chosenPoison].tweenZ = null;
  }
  graphics.poisonModule[chosenPoison].part[1].position.y = 0;
  graphics.poisonModule[chosenPoison].part[1].rotation.z = 0;

  graphics.poisonModule[chosenPoison].scale.set(1, 1, 1);
	graphics.poisonModule[chosenPoison].position.set(-400 * side, -400, -400);
  graphics.poisonModule[chosenPoison].speed = [200 * side, 650];
  graphics.poisonModule[chosenPoison].stat = "solid";
  graphics.poisonModule[chosenPoison].tweenZ = gsap.fromTo(graphics.poisonModule[chosenPoison].rotation, { z: -0.5 }, { duration: 1 + Math.random() * 1, z: 0.5, ease: "power1.inOut", repeat: -1, yoyo: true });

  setTimeout(function() {
  	dropPoison();
  }, 6000);
}



let productZOrder = [true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true];


function dropProduct() {
	let newProduct = true;
	let productFull = false;
	let productCount = 0;
  for (let i = 0; i < graphics.productArray.length; i++) {
  	if (graphics.productArray[i][4] == "solid") {
  		productCount++;
  	}
  }
  if (productCount < 10) {
    if (newProduct) {
    	graphics.productArray[graphics.productArray.length] = [];
  	
  	  graphics.productArray[graphics.productArray.length - 1][8] = Math.floor(Math.random() * 7);

      //graphics.productArray[graphics.productArray.length - 1][8] = 6;
      
  	  graphics.productArray[graphics.productArray.length - 1][0] = graphics.productModule[graphics.productArray[graphics.productArray.length - 1][8]].part[0].container.clone();
  	  graphics.productArray[graphics.productArray.length - 1][1] = graphics.productModule[graphics.productArray[graphics.productArray.length - 1][8]].part[1].container.clone();
  	
    	graphics.productArray[graphics.productArray.length - 1][2] = graphics.productArea[graphics.productArray[graphics.productArray.length - 1][8]].clone();

    }
    
    
    for (let i = 0; i < productZOrder.length; i++) {
    	if (productZOrder[i]) {
    		productZOrder[i] = false;
    		graphics.productArray[graphics.productArray.length - 1][5] = i;
    		break;
    	}
    }

    
    const side = -1 + Math.round(Math.random()) * 2;
    
    
        
  	graphics.productArray[graphics.productArray.length - 1][0].position.set(400 * side, -400, -5000 - 200 * graphics.productArray[graphics.productArray.length - 1][5]);
    graphics.productArray[graphics.productArray.length - 1][1].position.set(400 * side, -400, -5000 - 200 * graphics.productArray[graphics.productArray.length - 1][5]);
    graphics.productArray[graphics.productArray.length - 1][2].position.set(400 * side, -400, -5000 - 200 * graphics.productArray[graphics.productArray.length - 1][5]);
    
    
    if (graphics.productArray[graphics.productArray.length - 1][8] == 0) {
      graphics.productArray[graphics.productArray.length - 1][0].children[0].rotation.z = Math.random() * 6;
      graphics.productArray[graphics.productArray.length - 1][1].children[0].rotation.z = graphics.productArray[graphics.productArray.length - 1][0].children[0].rotation.z;
      graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.55, 0.55, 0.55);
      graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.55, 0.55, 0.55);
      graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.55, 0.55, 0.55);

    } else if (graphics.productArray[graphics.productArray.length - 1][8] == 1) {
    	graphics.productArray[graphics.productArray.length - 1][0].children[0].rotation.y = Math.random() * 6;
    	graphics.productArray[graphics.productArray.length - 1][1].children[0].rotation.y = graphics.productArray[graphics.productArray.length - 1][0].children[0].rotation.y;
      graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.55, 0.55, 0.55);
      graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.55, 0.55, 0.55);
      graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.55, 0.55, 0.55);

    } else if (graphics.productArray[graphics.productArray.length - 1][8] == 2) {
     	graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.29, 0.29, 0.29);
    	graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.29, 0.29, 0.29);
    	graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.29, 0.29, 0.29);
    
    } else if (graphics.productArray[graphics.productArray.length - 1][8] == 3) {
    	graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.38, 0.38, 0.38);
    	graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.38, 0.38, 0.38);
    	graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.38, 0.38, 0.38);
    
    } else if (graphics.productArray[graphics.productArray.length - 1][8] == 4) {
    	graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.45, 0.45, 0.45);
    	graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.45, 0.45, 0.45);
    	graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.45, 0.45, 0.45);
    
    } else if (graphics.productArray[graphics.productArray.length - 1][8] == 5) {
    	graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.41, 0.41, 0.41);
    	graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.41, 0.41, 0.41);
    	graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.41, 0.41, 0.41);
    
    } else if (graphics.productArray[graphics.productArray.length - 1][8] == 6) {
    	graphics.productArray[graphics.productArray.length - 1][0].scale.set(0.34, 0.34, 0.34);
    	graphics.productArray[graphics.productArray.length - 1][1].scale.set(0.34, 0.34, 0.34);
    	graphics.productArray[graphics.productArray.length - 1][2].scale.set(0.34, 0.34, 0.34);
    
    }
    
    
    
    graphics.productArray[graphics.productArray.length - 1][3] = [(-100 - Math.random() * 150) * side, 650 + Math.random() * 180];
    graphics.productArray[graphics.productArray.length - 1][7] = [graphics.productArray[graphics.productArray.length - 1][3][0], graphics.productArray[graphics.productArray.length - 1][3][1]];

   
    graphics.productArray[graphics.productArray.length - 1][4] = "solid";
    
    
    
    graphics.productArray[graphics.productArray.length - 1].tweenY = gsap.to([graphics.productArray[graphics.productArray.length - 1][0].rotation, graphics.productArray[graphics.productArray.length - 1][1].rotation, graphics.productArray[graphics.productArray.length - 1][2].rotation], { duration: 1 + Math.random() * 1, y: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
    graphics.productArray[graphics.productArray.length - 1].tweenZ = gsap.to([graphics.productArray[graphics.productArray.length - 1][0].rotation, graphics.productArray[graphics.productArray.length - 1][1].rotation, graphics.productArray[graphics.productArray.length - 1][2].rotation], { duration: 1 + Math.random() * 1, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
  

    graphics.productArray[graphics.productArray.length - 1][6] = [0, Math.PI * 0.5];
  
    mainScene.add(graphics.productArray[graphics.productArray.length - 1][0], graphics.productArray[graphics.productArray.length - 1][1], graphics.productArray[graphics.productArray.length - 1][2]);
  }
  
  
  setTimeout(function() {
  	dropProduct();
  }, 1000);


}
function cutPoison(i) {
	
	graphics.poisonModule[i].tweenZ.pause();
  gsap.to(graphics.poisonModule[i].part[1].position, { duration: 0.05, y: 30, ease: "power2.out" });
  gsap.to(graphics.poisonModule[i].part[1].rotation, { duration: 0.05, z: -1 + Math.random() * 2, ease: "power2.out" });

  gsap.fromTo(graphics.poisonModule[i].rotation, { z: graphics.poisonModule[i].part[0].rotation.z - 0.2 }, { duration: 0.07, z: graphics.poisonModule[i].part[0].rotation.z + 0.2, ease: "power1.inOut", repeat: 5, yoyo: true, onComplete() {
  	gsap.to(graphics.poisonModule[i].scale, { duration: 0.05, x: 0, y: 0, ease: "back.in" });
    graphics.poisonModule[i].FX_1.scale.set(0, 0, 1);
    graphics.poisonModule[i].FX_1.material.opacity = 1;
    graphics.poisonModule[i].FX_1.rotation.z = Math.random() * Math.PI * 2;
    graphics.poisonModule[i].FX_1.position.set(graphics.poisonModule[i].position.x, graphics.poisonModule[i].position.y, 5.5);
    graphics.poisonModule[i].FX_2.scale.set(0, 0, 1);
    graphics.poisonModule[i].FX_2.material.opacity = 1;
    graphics.poisonModule[i].FX_2.rotation.z = Math.random() * Math.PI * 2;
    graphics.poisonModule[i].FX_2.position.set(graphics.poisonModule[i].position.x, graphics.poisonModule[i].position.y, 5.4);
    gsap.to(graphics.poisonModule[i].FX_1.scale, { duration: 0.2, x: 1, y: 1, ease: "back.out", onComplete() {
    	gsap.to(graphics.poisonModule[i].FX_1.scale, { duration: 0.5, x: 1.2, y: 1.2, ease: "power1.out" });

    	gsap.to(graphics.poisonModule[i].FX_1.material, { duration: 0.5, opacity: 0, ease: "power1.inOut", onComplete() {
        mainScene.remove(graphics.poisonModule[i].FX_1);
    	} });
    } });
    gsap.to(graphics.poisonModule[i].FX_2.scale, { duration: 0.3, x: 1, y: 1, ease: "back.out", onComplete() {
    	gsap.to(graphics.poisonModule[i].FX_2.scale, { duration: 0.7, x: 1.5, y: 1.5, ease: "power1.out" });

    	gsap.to(graphics.poisonModule[i].FX_2.material, { duration: 0.7, opacity: 0, ease: "power1.inOut", onComplete() {
        mainScene.remove(graphics.poisonModule[i].FX_2);
    	} });
    } });
    mainScene.add(graphics.poisonModule[i].FX_1, graphics.poisonModule[i].FX_2);
  } });

}


function cutProduct(object) {
	graphics.productArray[object].tweenY.kill();
	graphics.productArray[object].tweenY = null;
	graphics.productArray[object].tweenZ.kill();
	graphics.productArray[object].tweenZ = null;
  const zAngle = moveAngle;
  
  
  let faceProduct;
  
  if (graphics.productArray[object][8] == 0) {
  	faceProduct = [Math.PI * 0.5, zAngle + Math.PI * 0.5, 0];
  	graphics.productArray[object][3][0] -= 200 * Math.cos(zAngle);
  	graphics.productArray[object][3][1] -= 300 * Math.sin(zAngle);
  	graphics.productArray[object][7][0] += 200 * Math.cos(zAngle);
  	graphics.productArray[object][7][1] += 300 * Math.sin(zAngle);

  } else if (graphics.productArray[object][8] == 1) {
  	faceProduct = [0, 0, zAngle + Math.PI * 0.5];
  	graphics.productArray[object][3][0] -= 200 * Math.cos(zAngle);
  	graphics.productArray[object][3][1] -= 300 * Math.sin(zAngle);
  	graphics.productArray[object][7][0] += 200 * Math.cos(zAngle);
  	graphics.productArray[object][7][1] += 300 * Math.sin(zAngle);
  	
  	  	

  } else if (graphics.productArray[object][8] == 2) {
  	faceProduct = [0, 0, zAngle];
    graphics.productArray[object][3][0] += 200 * Math.cos(zAngle);
    graphics.productArray[object][3][1] += 300 * Math.sin(zAngle);
    graphics.productArray[object][7][0] -= 200 * Math.cos(zAngle);
    graphics.productArray[object][7][1] -= 300 * Math.sin(zAngle);
  
  
  
  } else if (graphics.productArray[object][8] == 3) {
  	faceProduct = [0, 0, zAngle];
  	graphics.productArray[object][3][0] -= 200 * Math.cos(zAngle);
  	graphics.productArray[object][3][1] -= 300 * Math.sin(zAngle);
  	graphics.productArray[object][7][0] += 200 * Math.cos(zAngle);
  	graphics.productArray[object][7][1] += 300 * Math.sin(zAngle);
  
  
  
  } else if (graphics.productArray[object][8] == 4) {
  	faceProduct = [0, 0, zAngle + Math.PI * 0.5];
  	graphics.productArray[object][3][0] += 200 * Math.cos(zAngle);
  	graphics.productArray[object][3][1] += 300 * Math.sin(zAngle);
  	graphics.productArray[object][7][0] -= 200 * Math.cos(zAngle);
  	graphics.productArray[object][7][1] -= 300 * Math.sin(zAngle);
  	
  	
  } else if (graphics.productArray[object][8] == 5) {
  	faceProduct = [0, 0, zAngle];
  	graphics.productArray[object][3][0] -= 200 * Math.cos(zAngle);
  	graphics.productArray[object][3][1] -= 300 * Math.sin(zAngle);
  	graphics.productArray[object][7][0] += 200 * Math.cos(zAngle);
  	graphics.productArray[object][7][1] += 300 * Math.sin(zAngle);
  
  
  
  } else if (graphics.productArray[object][8] == 6) {
  	faceProduct = [0, 0, zAngle];
  	graphics.productArray[object][3][0] -= 200 * Math.cos(zAngle);
  	graphics.productArray[object][3][1] -= 300 * Math.sin(zAngle);
  	graphics.productArray[object][7][0] += 200 * Math.cos(zAngle);
  	graphics.productArray[object][7][1] += 300 * Math.sin(zAngle);
  
  
  
  }
  
  
  gsap.to([graphics.productArray[object][0].rotation, graphics.productArray[object][1].rotation], { duration: 0, x: faceProduct[0], y: faceProduct[1], z: faceProduct[2], ease: "power1.inOut", onComplete: function() {
		
		if (graphics.productArray[object][8] == 0) {
	  	graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
      graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: graphics.productArray[object][0].children[0].rotation.z + Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
      graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
      graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: graphics.productArray[object][1].children[0].rotation.z + Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
		} else if (graphics.productArray[object][8] == 1) {
			graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, y: graphics.productArray[object][0].children[0].rotation.y + Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
      graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
      graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, y: graphics.productArray[object][1].children[0].rotation.y + Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
      graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });

		} else if (graphics.productArray[object][8] == 2) {
			graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
		
		} else if (graphics.productArray[object][8] == 3) {
			graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
		
		} else if (graphics.productArray[object][8] == 4) {
			graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
		
		} else if (graphics.productArray[object][8] == 5) {
			graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
		
		} else if (graphics.productArray[object][8] == 6) {
			graphics.productArray[object][0].tweenY = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][0].tweenZ = gsap.to(graphics.productArray[object][0].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenY = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, x: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
			graphics.productArray[object][1].tweenZ = gsap.to(graphics.productArray[object][1].children[0].rotation, { duration: 0.8 + Math.random() * 0.6, z: Math.PI * (2 - Math.round(Math.random()) * 4), ease: "none", repeat: -1 });
		
		}

	} });
	
	


	
}

let onTouch = false;
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();

function onDocumentTouchStart(event) {
  event.preventDefault();
  onTouch = true;
  mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
    
  raycaster.setFromCamera(mouse, mainCamera);
  
  if (event.touches.length == 1 && onTrail) {
  	if (onTutorial == 1) graphics.hintTween.pause();
    const tempTranslationMatrix = new THREE.Matrix4();
    tempTranslationMatrix.makeTranslation(mouse.x * window.innerWidth / mainScene.scale.x / 2, mouse.y * window.innerHeight / mainScene.scale.x / 2, 0);
    const tempRotationMatrix = new THREE.Matrix4();
    trailTarget.matrix.identity();
    trailTarget.applyMatrix4(tempTranslationMatrix);
    trailTarget.updateMatrixWorld();
    trailTarget.oldPos = [mouse.x, mouse.y];
    trail.initialize(trailMaterial, Math.round(20 * fps / 120), 0, 0, headGeometry, trailTarget);
    trail.activate();
  }
    
  
  
  if (event.touches.length == 1) checkUserAction();
}


function checkUserAction() {
  if (raycaster.intersectObject(graphics.button[0].clickableArea).length > 0 && graphics.button[0].ready) hideStartScreen();
  if (raycaster.intersectObject(graphics.button[1].clickableArea).length > 0 && graphics.button[1].ready) hideTutorial();

  		//goCutFX(0, 0, 1);
}
function onDocumentTouchEnd() {
	
//	gsap.to([trailMaterial.uniforms.headColor.value, trailMaterial.uniforms.tailColor.value], { duration: 0.1, w: 0, ease: "none" });


	if (onTutorial == 1) {
		graphics.hintTween.kill();
		graphics.hintTween = null;
		showHint();
	}
}

let moveAngle;
function onDocumentTouchMove(event) {
	
	
  event.preventDefault();
  
  mouse.x = (event.changedTouches[0].clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, mainCamera);

  if (event.touches.length == 1) {
  	
  	if (onTrail) {
  		
      const tempTranslationMatrix = new THREE.Matrix4();
      tempTranslationMatrix.makeTranslation(mouse.x * window.innerWidth / mainScene.scale.x / 2, mouse.y * window.innerHeight / mainScene.scale.x / 2, 0);
      const tempRotationMatrix = new THREE.Matrix4();
      moveAngle = new THREE.Vector2(mouse.x - trailTarget.oldPos[0], (mouse.y - trailTarget.oldPos[1]) / window.innerWidth * window.innerHeight).angle() - Math.PI * 0.5;
      tempRotationMatrix.makeRotationZ(moveAngle);
      tempTranslationMatrix.multiply(tempRotationMatrix);
      trailMaterial.uniforms.headColor.value.w = new THREE.Vector2(mouse.x, mouse.y).distanceTo(new THREE.Vector2(trailTarget.oldPos[0], trailTarget.oldPos[1])) / 0.15 * (window.innerWidth * window.devicePixelRatio / 1602);
      if (trailMaterial.uniforms.headColor.value.w > 0.5) {
    	  trailMaterial.uniforms.headColor.value.w = 0.5;
      }
      trailTarget.oldPos = [mouse.x, mouse.y];
      trailTarget.matrix.identity();
      trailTarget.applyMatrix4(tempTranslationMatrix);
      trailTarget.updateMatrixWorld();
      for (let i = 0; i < graphics.productArray.length; i++) {
        if (graphics.productArray[i][4] == "solid" && raycaster.intersectObject(graphics.productArray[i][2]).length > 0) {
    	    graphics.productArray[i][4] = "cut";
    	    goCutFX(mouse.x * window.innerWidth / mainScene.scale.x / 2, mouse.y * window.innerHeight / mainScene.scale.x / 2, moveAngle + Math.PI * 0.5, true);
    	    cutProduct(i);
    	    if (onTutorial == 1) {
    	    	graphics.hintTween.kill();
    	    	graphics.hintTween = null;
    	    	onTutorial = 2;
    	    	onFly = true;
    	    	const tempTranslationMatrix = new THREE.Matrix4();
    	    	tempTranslationMatrix.makeTranslation(mouse.x * window.innerWidth / mainScene.scale.x / 2, mouse.y * window.innerHeight / mainScene.scale.x / 2, 0);
    	    	const tempRotationMatrix = new THREE.Matrix4();
    	    	trailTarget.matrix.identity();
    	    	trailTarget.applyMatrix4(tempTranslationMatrix);
    	    	trailTarget.updateMatrixWorld();
    	    	trailTarget.oldPos = [mouse.x, mouse.y];
    	    	trail.initialize(trailMaterial, Math.round(20 * fps / 120), 0, 0, headGeometry, trailTarget);
    	    	trail.activate();
    	    	onTrail = false;
    	    	showHint_2();
    	    }
    	    break;
        }
      }
      for (let i = 0; i < graphics.poisonModule.length; i++) {
        if (graphics.poisonModule[i].stat == "solid" && raycaster.intersectObject(graphics.poisonModule[i].area).length > 0) {
        	graphics.poisonModule[i].stat = "cut";
    	    goCutFX(mouse.x * window.innerWidth / mainScene.scale.x / 2, mouse.y * window.innerHeight / mainScene.scale.x / 2, moveAngle + Math.PI * 0.5, false);
    	    cutPoison(i);
          break;
        }
  
      }
  	}
  }
  
}









const clock = new THREE.Clock();
clock.start();
let fpsSum = 0;
let frameStep = 0;
let frameTime = 0;
let currentFps = 0;

let fps;

loop();
function loop() {
	
  if (oldWindow !== document.body.clientWidth / document.body.clientHeight) onWindowResize();
  mainRenderer.render(mainScene, mainCamera);
  
  const delta = clock.getDelta();
  if (onFly) {
  	for (let i = 0; i < graphics.productArray.length; i++) {
  		if (graphics.productArray[i][4] == "solid" || graphics.productArray[i][4] == "cut") {
  		  if (onTutorial == 0 && graphics.productArray[i][4] == "solid" && graphics.productArray[i][2].position.x + graphics.productArray[i][3][0] * delta > 0) {
  		  	graphics.productArray[i][0].position.x = 0;
  	  	  graphics.productArray[i][1].position.x = 0;
  	  	  graphics.productArray[i][2].position.x = 0;
  	  	  graphics.productArray[i].tweenY.pause();
  	  	  graphics.productArray[i].tweenZ.pause();
  	  	  onFly = false;
  	  	  onTutorial = 1;
  	  	  showHint();
  		  } else {
  		    graphics.productArray[i][0].position.x += graphics.productArray[i][3][0] * delta;
  	    	graphics.productArray[i][1].position.x += graphics.productArray[i][7][0] * delta;
  	    	graphics.productArray[i][2].position.x += graphics.productArray[i][3][0] * delta;
  		  }
  		  graphics.productArray[i][0].position.y += graphics.productArray[i][3][1] * delta;
  		  graphics.productArray[i][1].position.y += graphics.productArray[i][7][1] * delta;
  		  graphics.productArray[i][2].position.y += graphics.productArray[i][3][1] * delta;
  		  graphics.productArray[i][3][1] -= 500 * delta;
  		  graphics.productArray[i][7][1] -= 500 * delta;
  		  
  		  
  		  	
  		  
  		  if (graphics.productArray[i][0].position.y < -600 && graphics.productArray[i][1].position.y < -600) {
  		  	graphics.productArray[i][4] = "out";
  		  	productZOrder[graphics.productArray[i][5]] = true;
  		  }
  		}
  	}
  	
  	for (let i = 0; i < graphics.poisonModule.length; i++) {
  		if (graphics.poisonModule[i].stat == "solid") {
  			if (i == 0 && onTutorial == 2 && graphics.poisonModule[i].stat == "solid" && graphics.poisonModule[i].position.x + graphics.poisonModule[i].speed[0] * delta > -60) {
  				graphics.poisonModule[i].position.x = -60;
  				graphics.poisonModule[1].position.x = 60;
  				
  	
  				graphics.poisonModule[i].tweenZ.pause();
  				graphics.poisonModule[1].tweenZ.pause();
  				onFly = false;
  				onTutorial = 3;
  				
  				showHint_3();
  			} else {
  				graphics.poisonModule[i].position.x += graphics.poisonModule[i].speed[0] * delta;
   			}
  			graphics.poisonModule[i].position.y += graphics.poisonModule[i].speed[1] * delta;
   			graphics.poisonModule[i].speed[1] -= 500 * delta;
  			
  	
  			if (graphics.poisonModule[i].position.y < -600) {
  				graphics.poisonModule[i].stat = "out";
  			}
  		}
  	}
  }
  
  frameTime = (frameTime + delta) / 2;
  currentFps = (currentFps + 1 / frameTime) / 2;
  fpsSum += currentFps;
  frameStep ++;
  fps = Math.round(fpsSum / frameStep);
  
  
  //clock.getDelta()
  //console.log(0.0083 / clock.getDelta())
  if (onTrail && onTouch) {
  	//console.log(300 * clock.getDelta() * 1000)
  	//trail.setAdvanceFrequency(300 * clock.getDelta() * 1000);
  	
  	//trail.length = 20 * 0.0083 / clock.getDelta()
  	trail.update();
  	
  }
  requestAnimationFrame(loop);
}
