import * as THREE from './three.module.min.js';

import { TrailRenderer } from './TrailRenderer.js';


const mainRenderer = new THREE.WebGLRenderer({ antialias: true, powerPreference: "high-performance" });
mainRenderer.outputColorSpace = THREE.SRGBColorSpace;
mainRenderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(mainRenderer.domElement);
const mainScene = new THREE.Scene();
mainScene.background = new THREE.Color(0x69F0AE);
const ambientLight = new THREE.AmbientLight(0xFFFFFF, 0.6);
const directionalLight = new THREE.DirectionalLight(0xFFFFFF, 0.8);
directionalLight.position.set(-1, 1, 1.5);
mainScene.add(ambientLight, directionalLight);
const mainCamera = new THREE.OrthographicCamera(document.body.clientWidth / -2, window.innerWidth / 2, document.body.clientHeight / 2, document.body.clientHeight / -2, 0.01, 1000);
mainCamera.position.set(0, 0, 10);
onWindowResize();
function onWindowResize() {
	if (document.body.clientHeight / document.body.clientWidth < 16 / 9) {
	  mainScene.scale.set(document.body.clientHeight / 640, document.body.clientHeight / 640, document.body.clientHeight / 640);
	} else {
		mainScene.scale.set(document.body.clientWidth / 360, document.body.clientWidth / 360, document.body.clientWidth / 360);
	}
	mainRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
	mainCamera.left = -document.body.clientWidth / 2;
	mainCamera.right = document.body.clientWidth / 2;
	mainCamera.top = document.body.clientHeight / 2;
	mainCamera.bottom = -document.body.clientHeight / 2;
	mainCamera.updateProjectionMatrix();
}



//initScene();
// initGUI();
//initListeners();
//initLights();

//function initSceneGeometry(onFinished) {
	
    animate();
    
		

function animate() {
    requestAnimationFrame(animate);
    
    render();
}





function render() {
	trail.update();
  mainRenderer.render(mainScene, mainCamera);
}









document.body.addEventListener('touchmove', onDocumentTouchMove, false);














const mouse = {};
function onDocumentTouchMove(event) {
  event.preventDefault();
  mouse.x = -(event.changedTouches[0].clientX / window.innerWidth) * 2 + 1;
  mouse.y = -(event.changedTouches[0].clientY / window.innerHeight) * 2 + 1;
	trail.update();
	const tempTranslationMatrix = new THREE.Matrix4();
  tempTranslationMatrix.makeTranslation (-mouse.x * 360 / mainScene.scale.x, mouse.y * 360, -100);
  const tempRotationMatrix = new THREE.Matrix4();
  tempRotationMatrix.makeRotationZ(1);
  tempTranslationMatrix.multiply(tempRotationMatrix);  
  trailTarget.matrix.identity();
  trailTarget.applyMatrix4(tempTranslationMatrix);
  trailTarget.updateMatrixWorld();
}
